import axios from "axios";

const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;


export const postCheckout = async (token, data) => {
  const response = await axios.post(`${apiUrl}/checkout`, data,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}
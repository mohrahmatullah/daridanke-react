import axios from "axios";

const username = import.meta.env.VITE_REACT_APP_API_USERNAME_DASH;
const password = import.meta.env.VITE_REACT_APP_API_PASSWORD_DASH;
const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;

export const getShop = async (page, search, sort , category) => {
  const response = await axios.get(`${apiUrl}/shop?page=${page}&search=${search}&sort=${sort}&category=${category}`,{
    auth: {
      username: username,
      password: password
    }
  });

  return response.data;
}

export const getTopShop = async () => {
  const response = await axios.get(`${apiUrl}/shop/list/top`,{
    auth: {
      username: username,
      password: password
    }
  });

  return response.data;
}

export const getRekomendasiShop = async () => {
  const response = await axios.get(`${apiUrl}/shop/list/rekomendasi`,{
    auth: {
      username: username,
      password: password
    }
  });

  return response.data;
}

export const getShopById = async (id) => {
  const response = await axios.get(`${apiUrl}/shop/${id}`,{
    auth: {
      username: username,
      password: password
    }
  });

  return response.data;
}

export const getCategory = async () => {
  const response = await axios.get(`${apiUrl}/shop/category/all`,{
    auth: {
      username: username,
      password: password
    }
  });

  return response.data;
}
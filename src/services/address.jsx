import axios from "axios";

const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;


export const getProvincies = async (token) => {
  const response = await axios.get(`${apiUrl}/address/provincies`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const getCities = async (token, prov_id) => {
  const response = await axios.get(`${apiUrl}/address/cities/${prov_id}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const getDistrict = async (token, city_id) => {
  const response = await axios.get(`${apiUrl}/address/district/${city_id}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const getSubDistrict = async (token, dis_id) => {
  const response = await axios.get(`${apiUrl}/address/subdistrict/${dis_id}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const getPostalCode = async (token, subdis_id) => {
  const response = await axios.get(`${apiUrl}/address/postal_code/${subdis_id}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const createAddress = async (token, data) => {
  const response = await axios.post(`${apiUrl}/address`, data,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const geAddressByUser = async (token, data, page) => {
  const response = await axios.get(`${apiUrl}/address/${data.id_user}?page=${page}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const geAddressByUserIdAddress = async (token, data) => {
  const response = await axios.get(`${apiUrl}/address/is_active/${data.id_user}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const changeDefaultAddress = async (token, id, data) => {
  const response = await axios.post(`${apiUrl}/address/change_is_active/${id}`, data, 
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}
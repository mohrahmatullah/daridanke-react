import axios from "axios";

const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;


export const getOrder = async (page, search, decode, token) => {
  const response = await axios.get(`${apiUrl}/order?page=${page}&search=${search}&id_user=${decode.id}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}

export const getOrderByID = async (id, token) => {
  const response = await axios.get(`${apiUrl}/order/${id}`,
  {
    headers: {
      Authorization: `Bearer ${token}`
    }
  }
  );

  return response.data;
}
import axios from "axios";

const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;


export const createPaymentConfirmation = async (decode, id, token, paymentData) => {
  const formData = new FormData();
  formData.append('Id_post', id);
  formData.append('Id_customer', decode.id);
  formData.append('Name_bank', paymentData.Name_bank);
  formData.append('No_rek', paymentData.No_rek);
  formData.append('Evidence', paymentData.Evidence);
  try {
    const response = await axios.post(
      `${apiUrl}/paymentConfirmation`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`
        }
      }
    );

    return response.data;
  } catch (error) {
    console.error('Error creating payment confirmation:', error);
    throw error;
  }
};

export const getPaymentConfirmationByID = async (id, token) => {
  try {
    const response = await axios.get(`${apiUrl}/paymentConfirmation/${id}`,
    {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }
    );

    return response.data;
  } catch (error) {
    console.error('Error creating payment confirmation:', error);
    throw error;
  }
}
import axios from "axios";

const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;
const basic_username = import.meta.env.VITE_REACT_APP_API_USERNAME_DASH;
const basic_password = import.meta.env.VITE_REACT_APP_API_PASSWORD_DASH;

export const postLogin = async (username,password) => {
  try {
    const response = await axios.post(`${apiUrl}/auth/loginUser`, {
      username: username,
      password: password,
    });

    return response.data;
  } catch (error) {
    return { error };
  }
}


export const postRegister = async (email, username, password ) => {
  const response = await axios.post(`${apiUrl}/register/registerUser`,
  {
    email: email,
    username: username,
    password: password
  },
  {
    auth: {
      username: basic_username,
      password: basic_password
    }
  });

  return response.data;
}
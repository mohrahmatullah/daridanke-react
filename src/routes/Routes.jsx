import React from 'react';
import { BrowserRouter , Routes, Route } from 'react-router-dom';
import ScrollToTop from '@/components/ScrollToTop';
import Home from '../pages/Home';
import Shop from '../pages/Shop';
import Login from '../pages/Login';
import Account from '../pages/Account';
import Checkout from '../pages/Checkout';
import ThankYou from '../pages/ThankYou';
import Product from '../pages/Product';
import Order from '../pages/Order';
import Invoice from '../pages/Invoice';
import PaymentConfirmation from '../pages/PaymentConfirmations';
import Wishlist from '../pages/wishlist';
import Voucher from '../pages/Voucher';
import AboutAs from '../pages/AboutAs';
import Contact from '../pages/Contact';
import Profile from '../pages/Profile';
import Address from '../pages/Address'; 
import CreateAddress from '../pages/CreateAddress'; 
import Register from '../pages/Register';

const Router = () => {
  return (
    <BrowserRouter>
      <ScrollToTop />
      <Routes>
        <Route path="/" element={<Home />} /> 
        <Route path="shop" element={<Shop />} /> 
        <Route path="login" element={<Login />} /> 
        <Route path="account" element={<Account />} />
        <Route path="address" element={<Address />} />
        <Route path="create-address" element={<CreateAddress />} />
        <Route path="checkout" element={<Checkout />} />
        <Route path="thankyou/:id" element={<ThankYou />} /> 
        <Route path="product/:id" element={<Product />} />
        <Route path="wishlist" element={<Wishlist />} />
        <Route path="voucher" element={<Voucher />} />
        <Route path="about-as" element={<AboutAs />} />
        <Route path="contact" element={<Contact />} />
        <Route path="order/:search" element={<Order />} />
        <Route path="invoice/:id" element={<Invoice />} />
        <Route path="payment-confirmation/:id" element={<PaymentConfirmation />} />
        <Route path="profile" element={<Profile />} />
        <Route path="register" element={<Register />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;

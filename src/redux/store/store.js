// store.js
import { createStore, combineReducers } from 'redux';
import cartReducer from '../reducers/cartReducer';

const rootReducer = combineReducers({
  cart: cartReducer,
  // Other reducers if you have more state slices
});

const store = createStore(rootReducer);

export default store;
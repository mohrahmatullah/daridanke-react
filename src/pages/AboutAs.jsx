import React, { useEffect } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import user_icon from "@/assets/user.png";
import director from "@/assets/director.png";
import groupCompany from "@/assets/groupCompany.jpg";

const AboutAs = () => {
  useEffect(() => {
    document.title = "Daridanke | Tentang Kami";
  }, []);

  return (
    <>
      <Header />
      
      {/* Hero Section */}
      <section className="relative w-full h-[60vh] bg-gray-900 flex items-center justify-center">
        <div className="absolute inset-0">
          <img src={groupCompany} alt="Group Company" className="w-full h-full object-cover opacity-50" />
        </div>
        <h1 className="relative text-white text-4xl lg:text-6xl font-extrabold text-center">
          Tentang Kami
        </h1>
      </section>

      {/* Content Section */}
      <div className="container mx-auto py-20 px-6 lg:px-16">
        <div className="flex flex-col lg:flex-row items-center gap-12">
          {/* About Us Text */}
          <div className="lg:w-1/2">
            <h2 className="text-3xl lg:text-4xl font-bold text-gray-900">Siapa Kami?</h2>
            <p className="mt-4 text-lg text-gray-600">
              Daridanke adalah platform digital yang menyediakan berbagai template siap pakai untuk kebutuhan profesional dan bisnis.
              Kami menghadirkan solusi terbaik dengan template dokumen, administrasi, dan bisnis yang dibuat oleh para profesional.
            </p>
          </div>

          {/* Image Section */}
          <div className="lg:w-1/2">
            <img src={groupCompany} alt="Tentang Kami" className="w-full rounded-lg shadow-lg" />
          </div>
        </div>

        {/* Our Story Section */}
        <div className="mt-20 flex flex-col lg:flex-row-reverse items-center gap-12">
          {/* Our Story Text */}
          <div className="lg:w-1/2">
            <h2 className="text-3xl lg:text-4xl font-bold text-gray-900">Perjalanan Kami</h2>
            <p className="mt-4 text-lg text-gray-600">
              Bermula dari kesulitan mendapatkan template dokumen yang rapi dan profesional, kami membangun Daridanke untuk
              memberikan solusi digital yang praktis, efisien, dan mudah digunakan. Kami terus berinovasi untuk mendukung produktivitas bisnis dan individu.
            </p>
          </div>

          {/* Image Section */}
          <div className="lg:w-1/2">
            <img src={groupCompany} alt="Our Story" className="w-full rounded-lg shadow-lg" />
          </div>
        </div>
      </div>

      {/* Team Section */}
      <section className="bg-gray-100 py-20">
        <div className="container mx-auto text-center">
          <h2 className="text-3xl lg:text-4xl font-bold text-gray-900">Tim Kami</h2>
          <p className="mt-4 text-lg text-gray-600">
            Tim kami terdiri dari individu yang berpengalaman di bidang bisnis dan teknologi, berkomitmen untuk memberikan layanan terbaik.
          </p>

          {/* Team Cards */}
          <div className="mt-12 grid grid-cols-3 md:grid-cols-3 lg:grid-cols-3 gap-8">
            {/* Founder Card */}
            <div className="bg-white shadow-lg rounded-lg p-6 text-center transform transition hover:-translate-y-3 hover:shadow-xl">
              <img src={director} alt="Erik Rudiana" className="w-32 h-32 mx-auto rounded-full border-4 border-gray-200 shadow-md" />
              <h3 className="mt-4 text-xl font-semibold text-gray-900">Erik Rudiana</h3>
              <p className="text-gray-500 italic">Founder & Director</p>
              <p className="mt-2 text-gray-600 text-sm">Memimpin inovasi & strategi bisnis dengan fokus pada kualitas dan kepuasan pelanggan.</p>
            </div>

            {/* Co-Founder Card */}
            <div className="bg-white shadow-lg rounded-lg p-6 text-center transform transition hover:-translate-y-3 hover:shadow-xl">
              <img src={user_icon} alt="Rohmatuloh" className="w-32 h-32 mx-auto rounded-full border-4 border-gray-200 shadow-md" />
              <h3 className="mt-4 text-xl font-semibold text-gray-900">Rohmatuloh</h3>
              <p className="text-gray-500 italic">Co-Founder & Developer</p>
              <p className="mt-2 text-gray-600 text-sm">Mengembangkan sistem dan fitur inovatif untuk meningkatkan pengalaman pengguna.</p>
            </div>

            {/* Future Team Placeholder */}
            <div className="bg-white shadow-lg rounded-lg p-6 text-center transform transition hover:-translate-y-3 hover:shadow-xl">
              <div className="w-32 h-32 mx-auto rounded-full bg-gray-300 flex items-center justify-center text-gray-600">
                ?
              </div>
              <h3 className="mt-4 text-xl font-semibold text-gray-900">Mungkin Kamu?</h3>
              <p className="text-gray-500 italic">Bergabunglah dengan kami!</p>
              <p className="mt-2 text-gray-600 text-sm">Kami selalu mencari talenta berbakat untuk berkembang bersama.</p>
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
};

export default AboutAs;

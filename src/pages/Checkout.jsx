import React, { useEffect, useState } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import { Link, useNavigate } from "react-router-dom";

import { formatCurrency } from '../helpers/helpers'; 
import { useSelector, useDispatch } from 'react-redux';
import { removeFromCart, clearCart } from '../redux/actions/actions';
import avatar from './../../public/assets/images/avatar.png'
import product5 from './../../public/assets/images/products/product5.jpg'
import product6 from './../../public/assets/images/products/product6.jpg'
import product10 from './../../public/assets/images/products/product10.jpg'
import { postCheckout } from '../services/checkout';
import { geAddressByUserIdAddress } from '../services/address';
import jwt_decode from "jwt-decode";

const Checkout = () => {

    const [cartItems, setCartItems] = useState([]);
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [address, setAddress] = useState('');
    const token = localStorage.getItem('token');
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;

    useEffect(() => {
        // Mengambil cartItems dari localStorage saat komponen dimuat
        const storedCartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
        setCartItems(storedCartItems);
        viewAddress();
    }, []);

    const viewAddress = async () => {
      
        try {  
          if (token) {
            const decode = jwt_decode(token);
            let data = {
              id_user       : decode.id
            }
            const response = await geAddressByUserIdAddress(token, data);
            setAddress(response.data)
          }     
  
        } catch (error) {
          console.error('Error fetching provinsi data:', error);
        }
    };

    const handleQuantityChange = (productId, amount) => {
        const updatedCart = cartItems.map(item => {
            if (item.ID === productId) {
                const newQuantity = item.quantity + amount;
                if(newQuantity > 0){
                    const newTotalPrice = item.Price * newQuantity; // Calculate the new total price
                    return { ...item, quantity: newQuantity, totalPrice: newTotalPrice };
                }
                else{
                    setAlertMessage('Quantity tidak boleh Nol');
                    setShowAlert(true);
                    setTimeout(() => {
                        setShowAlert(false);
                    }, 500);
                }
            }
            return item;
        });
        setCartItems(updatedCart);
        localStorage.setItem('cartItems', JSON.stringify(updatedCart));

    };

    const handleRemoveFromCart = (productId) => {
        // Update state dan localStorage setelah menghapus item dari keranjang
        const updatedCart = cartItems.filter(item => item.ID !== productId);
        setCartItems(updatedCart);
        localStorage.setItem('cartItems', JSON.stringify(updatedCart));

        setAlertMessage('Item deleted from cart successfully!');
        setShowAlert(true);
        setTimeout(() => {
            setShowAlert(false);
        }, 500);
    };

    const handleClearCart = () => {
        // Membersihkan keranjang dan localStorage
        setCartItems([]);
        localStorage.removeItem('cartItems');
        dispatch(clearCart());
    };

    const handleRedirect = () => {
        navigate('/account');
    };

    const fetchCheckout = async (decode, token) => {
        try {
            let order_data = localStorage.getItem('cartItems');
            let addressString = address ? `${address.Prov.ProvName}, ${address.City.CityName}, ${address.Dis.DisName}, ${address.Subdis.SubdisName}, ${address.Postal_code}` : '';
            let data = {
                Id_user: decode.id,
                Order_data: order_data,
                Address : addressString
            }
            const response = await postCheckout(token, data);
            const postId = response.data.Id_post;
            setAlertMessage(response.message);
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
                setAlertMessage('');
                localStorage.removeItem('cartItems');
                dispatch(clearCart());
                navigate(`/thankyou/${postId}`);
            }, 2000);

        } catch (error) {
            console.error('Error fetching login data:', error);
        }
    };

    const handlePlaceOrder = () => {
        // if(token){
        //     const decode = jwt_decode(token);
        //     fetchCheckout(decode,token);
        // }
        // else{
        //     setAlertMessage('Silahkan Login Terlebih Dahulu');
        //     setShowAlert(true);
        //     setTimeout(() => {
        //         setShowAlert(false);
        //         setAlertMessage('');
        //     }, 2000);
        // }

        try {
            setIsLoading(true); // Menandai dimulainya proses loading
            // Lakukan operasi place order di sini
            if (token) {
                if(address){
                    const decode = jwt_decode(token);
                    fetchCheckout(decode, token);
                }
                else{
                    setAlertMessage('Silahkan Tambahkan alamat dan Defaultkan alamat');
                    setShowAlert(true);
                    setTimeout(() => {
                        setShowAlert(false);
                        setAlertMessage('');
                    }, 2000);
                }
            } else {
                setAlertMessage('Silahkan Login Terlebih Dahulu');
                setShowAlert(true);
                setTimeout(() => {
                    setShowAlert(false);
                    setAlertMessage('');
                }, 2000);
            }
        } catch (error) {
            console.error('Error placing order:', error);
        } finally {
            setIsLoading(false); // Menghentikan proses loading
        }
    };

    const totalCartPrice = cartItems.reduce((total, item) => total + item.totalPrice, 0);

    return (
        <>
        <Header />

        <div className="container py-4 flex items-center gap-3">
            <Link to="/" className="text-primary text-base">
                <i className="fa-solid fa-house"></i>
            </Link>
            <span className="text-sm text-gray-400">
                <i className="fa-solid fa-chevron-right"></i>
            </span>
            <p className="text-gray-600 font-medium">Checkout</p>
        </div>
        

        {cartItems.length === 0 ? ( // Periksa apakah keranjang belanja kosong
            <div className="flex items-center justify-center h-screen">
                <div className="text-center">
                    <h2 className="text-3xl font-semibold mb-4">Your Cart is Empty</h2>
                    <p className="text-gray-500 mb-8">Looks like you haven't added any items to your cart yet.</p>
                    <Link to="/shop">
                        <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Shop Now</button>
                    </Link>                            
                </div>
            </div>
        ) : (
            <>
                <div className="container grid grid-cols-12 items-start pb-16 pt-4 gap-6">
                    <div className="col-span-8 border border-gray-200 p-4 rounded">
                        <div className="flex flex-col items-start pb-10">
                            {token ? (
                                <>
                                    <h3 className="text-lg font-medium capitalize mb-4">Address</h3>
                                    {token && address ? (
                                        <>                                        
                                            <span>{address.Prov.ProvName}, {address.City.CityName}, {address.Dis.DisName}, {address.Subdis.SubdisName}, {address.Postal_code}</span>
                                        </>                        
                                    ): (
                                        <>
                                            <div className="flex flex-col items-start pb-10 mt-4">
                                                <span className="mb-5">Silahkan Tambahkan Alamat dan Defaultkan Alamat</span>
                                                <Link to="/address" className="py-3 px-5 text-sm font-medium text-center rounded-lg bg-primary-700 border border-gray-300 sm:w-fit hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-primary-500 dark:focus:border-primary-500">Tambahkan</Link>
                                            </div>
                                        </>
                                    )}
                                </>    
                                ) : (
                                    <>
                                        <div className="flex flex-col items-start pb-10 mt-4">
                                            <span className="mb-5">Silahkan Login Terlebih Dahulu</span>
                                            <button className="py-3 px-5 text-sm font-medium text-center rounded-lg bg-primary-700 border border-gray-300 sm:w-fit hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-primary-500 dark:focus:border-primary-500" onClick={() => handleRedirect()}>Login</button>
                                        </div>
                                    </>
                                )
                            }                            
                        </div>
                        {/* <div className="space-y-4">
                            <div className="grid grid-cols-2 gap-4">
                                <div>
                                    <label for="first-name" className="text-gray-600">First Name <span
                                            className="text-primary">*</span></label>
                                    <input type="text" name="first-name" id="first-name" className="input-box" />
                                </div>
                                <div>
                                    <label for="last-name" className="text-gray-600">Last Name <span
                                            className="text-primary">*</span></label>
                                    <input type="text" name="last-name" id="last-name" className="input-box" />
                                </div>
                            </div>
                            <div>
                                <label for="company" className="text-gray-600">Company</label>
                                <input type="text" name="company" id="company" className="input-box" />
                            </div>
                            <div>
                                <label for="region" className="text-gray-600">Country/Region</label>
                                <input type="text" name="region" id="region" className="input-box" />
                            </div>
                            <div>
                                <label for="address" className="text-gray-600">Street address</label>
                                <input type="text" name="address" id="address" className="input-box" />
                            </div>
                            <div>
                                <label for="city" className="text-gray-600">City</label>
                                <input type="text" name="city" id="city" className="input-box" />
                            </div>
                            <div>
                                <label for="phone" className="text-gray-600">Phone number</label>
                                <input type="text" name="phone" id="phone" className="input-box" />
                            </div>
                            <div>
                                <label for="email" className="text-gray-600">Email address</label>
                                <input type="email" name="email" id="email" className="input-box" />
                            </div>
                            <div>
                                <label for="company" className="text-gray-600">Company</label>
                                <input type="text" name="company" id="company" className="input-box" />
                            </div>
                        </div> */}
                    <div className="flex justify-between items-center pb-10">
                        <h3 className="text-lg font-medium capitalize mb-4">Checkout</h3>
                        <button className="py-3 px-5 text-sm font-medium text-center rounded-lg bg-primary-700 border border-gray-300 sm:w-fit hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-primary-500 dark:focus:border-primary-500" onClick={() => handleClearCart()}>Remove All</button>
                    </div>
                    <div className="space-y-4">
                        {cartItems.map((item, index) => (
                        <div key={index} className="flex items-center justify-between border gap-6 p-4 border-gray-200 rounded">
                            <div className="w-28">
                            <img src={`${apiUrl}/files/${item.Image}`} alt={item.Name} className="w-full" />
                            </div>
                            <div className="w-1/3">
                            <h2 className="text-gray-800 text-xl font-medium uppercase">{item.Name}</h2>
                            <p className="text-gray-500 text-sm">SKU: <span className="text-green-600">{item.Sku}</span></p>
                            </div>
                            <div className="text-primary text-lg font-semibold">{formatCurrency(item.totalPrice)}</div>
                            
                            <div className="flex border border-gray-300 text-gray-600 divide-x divide-gray-300 w-max">
                                <div onClick={() => handleQuantityChange(item.ID, -1)} className="h-8 w-8 text-xl flex items-center justify-center cursor-pointer select-none">-</div>
                                <div className="h-8 w-8 text-base flex items-center justify-center">{item.quantity}</div>
                                <div onClick={() => handleQuantityChange(item.ID, 1)} className="h-8 w-8 text-xl flex items-center justify-center cursor-pointer select-none">+</div>
                            </div>

                            <div className="text-gray-600 cursor-pointer hover:text-primary" onClick={() => handleRemoveFromCart(item.ID)}>
                                <i className="fa-solid fa-trash"></i>
                            </div>
                        </div>
                        ))}
                    </div>
                    </div>

                    <div className="col-span-4 border border-gray-200 p-4 rounded">
                        <h4 className="text-gray-800 text-lg mb-4 font-medium uppercase">order summary</h4>
                        {/* <div className="space-y-2">
                            <div className="flex justify-between">
                                <div>
                                    <h5 className="text-gray-800 font-medium">Italian shape sofa</h5>
                                    <p className="text-sm text-gray-600">Size: M</p>
                                </div>
                                <p className="text-gray-600">
                                    x3
                                </p>
                                <p className="text-gray-800 font-medium">$320</p>
                            </div>
                            <div className="flex justify-between">
                                <div>
                                    <h5 className="text-gray-800 font-medium">Italian shape sofa</h5>
                                    <p className="text-sm text-gray-600">Size: M</p>
                                </div>
                                <p className="text-gray-600">
                                    x3
                                </p>
                                <p className="text-gray-800 font-medium">$320</p>
                            </div>
                            <div className="flex justify-between">
                                <div>
                                    <h5 className="text-gray-800 font-medium">Italian shape sofa</h5>
                                    <p className="text-sm text-gray-600">Size: M</p>
                                </div>
                                <p className="text-gray-600">
                                    x3
                                </p>
                                <p className="text-gray-800 font-medium">$320</p>
                            </div>
                            <div className="flex justify-between">
                                <div>
                                    <h5 className="text-gray-800 font-medium">Italian shape sofa</h5>
                                    <p className="text-sm text-gray-600">Size: M</p>
                                </div>
                                <p className="text-gray-600">
                                    x3
                                </p>
                                <p className="text-gray-800 font-medium">$320</p>
                            </div>
                        </div> */}

                        <div className="flex justify-between border-b border-gray-200 mt-1 text-gray-800 font-medium py-3 uppercas">
                            <p>Sub Total</p>
                            <p>{formatCurrency(totalCartPrice)}</p>
                        </div>

                        <div className="flex justify-between border-b border-gray-200 mt-1 text-gray-800 font-medium py-3 uppercas">
                            <p>Shipping</p>
                            <p>Free</p>
                        </div>

                        <div className="flex justify-between text-gray-800 font-medium py-3 uppercas">
                            <p className="font-semibold">Total</p>
                            <p>{formatCurrency(totalCartPrice)}</p>
                        </div>

                        <div className="flex items-center mb-4 mt-2">
                            <input type="checkbox" name="aggrement" id="aggrement" className="text-primary focus:ring-0 rounded-sm cursor-pointer w-3 h-3" />
                            <label htmlFor="aggrement" className="text-gray-600 ml-3 cursor-pointer text-sm">I agree to the <a href="#"
                                    className="text-primary">terms & conditions</a></label>
                        </div>

                        <button onClick={() => handlePlaceOrder()} className="block w-full py-3 px-4 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">Place
                            order</button>
                    </div>

                </div>
            </>
            
        )}

        
        {showAlert && (
            <div className="bg-red-500 text-white py-3 px-4 rounded-md absolute top-3 right-3">
                {alertMessage}
            </div>
        )}

        {isLoading && ( // Tampilkan halaman loading jika isLoading bernilai true
            <div className="fixed top-0 left-0 z-50 w-screen h-screen flex items-center justify-center bg-black bg-opacity-50">
                <div className="text-white text-3xl">Loading...</div>
            </div>
        )}
        
        <Footer />
        </>
    );
}

export default Checkout
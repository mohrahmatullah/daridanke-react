import React, { useState, useEffect } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { Link, useNavigate } from "react-router-dom";
import { postLogin } from "../services/auth";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    document.title = "Daridanke | Login";
    const token = localStorage.getItem("token");
    if (token) {
      navigate("/account");
    }
  }, []);

  const fetchLogin = async (username, password) => {
    try {
      const response = await postLogin(username, password);
      if (response && response.token) {
        localStorage.setItem("token", response.token);
        navigate("/account");
      } else if (
        response &&
        response.error &&
        response.error.response &&
        response.error.response.status === 401
      ) {
        setAlertMessage(response.error.response.data.message);
        setShowAlert(true);
        setTimeout(() => {
          setShowAlert(false);
        }, 1000);
        navigate("/login");
      } else {
        console.error("Token not found in response.");
        navigate("/login");
      }
    } catch (error) {
      console.error("Error fetching login data:", error);
    }
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    fetchLogin(username, password);
  };

  return (
    <>
      <Header />

      <div className="contain py-16">
        <div className="max-w-lg mx-auto shadow px-6 py-7 rounded overflow-hidden">
          <h2 className="text-2xl uppercase font-medium mb-1">Login</h2>
          {/* <p className="text-gray-600 mb-6 text-sm">
            welcome back customer
          </p> */}
          <form
            action="#"
            method="post"
            autoComplete="off"
            onSubmit={handleFormSubmit}
          >
            <div className="space-y-2">
              <div>
                <label htmlFor="username" className="text-gray-600 mb-2 block">
                  Username
                </label>
                <input
                  type="text"
                  name="username"
                  id="username"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  className="block w-full border border-gray-300 px-4 py-3 text-gray-600 text-sm rounded focus:ring-0 focus:border-primary placeholder-gray-400"
                  placeholder="Username"
                  autoComplete="username"
                />
              </div>
              <div>
                <label htmlFor="password" className="text-gray-600 mb-2 block">
                  Password
                </label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  className="block w-full border border-gray-300 px-4 py-3 text-gray-600 text-sm rounded focus:ring-0 focus:border-primary placeholder-gray-400"
                  placeholder="*******"
                  autoComplete="password"
                />
              </div>
            </div>
            <div className="flex items-center justify-between mt-6">
              <div className="flex items-center">
                <input
                  type="checkbox"
                  name="remember"
                  id="remember"
                  className="text-primary focus:ring-0 rounded-sm cursor-pointer"
                />
                <label
                  htmlFor="remember"
                  className="text-gray-600 ml-3 cursor-pointer"
                >
                  Remember me
                </label>
              </div>
              <a href="#" className="text-primary">
                Forgot password
              </a>
            </div>
            <div className="mt-4">
              <button
                type="submit"
                className="block w-full py-2 text-center text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium"
              >
                Login
              </button>
            </div>
          </form>

          <div className="mt-6 flex justify-center relative">
            <div className="text-gray-600 uppercase px-3 bg-white z-10 relative">
              Or login with
            </div>
            <div className="absolute left-0 top-3 w-full border-b-2 border-gray-200"></div>
          </div>
          {/* <div className="mt-4 flex gap-4">
            <a href="#"
              className="w-1/2 py-2 text-center text-white bg-blue-800 rounded uppercase font-roboto font-medium text-sm hover:bg-blue-700">facebook</a>
            <a href="#"
              className="w-1/2 py-2 text-center text-white bg-red-600 rounded uppercase font-roboto font-medium text-sm hover:bg-red-500">google</a>
          </div> */}

          <p className="mt-4 text-center text-gray-600">
            Don't have account?
            <Link to="/register" className="text-primary">
              {" "}
              Register now
            </Link>
          </p>
        </div>
      </div>

      {showAlert && (
        <div className="bg-red-500 text-white py-3 px-4 rounded-md absolute top-3 right-3">
          {alertMessage}
        </div>
      )}
      <Footer />
    </>
  );
};

export default Login;

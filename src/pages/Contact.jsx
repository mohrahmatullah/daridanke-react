import React, { useEffect } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import { FaEnvelope, FaMapMarkerAlt, FaPhoneAlt } from "react-icons/fa";
import groupCompany from "@/assets/groupCompany.jpg";

const ContactUs = () => {
  useEffect(() => {
    document.title = "Daridanke | Hubungi Kami";
  }, []);

  return (
    <>
      <Header />

      {/* Hero Section */}
      <section className="relative w-full h-[60vh] bg-gray-900 flex items-center justify-center">
        <div className="absolute inset-0">
          <img src={groupCompany} alt="Group Company" className="w-full h-full object-cover opacity-50" />
        </div>
        <h1 className="relative text-white text-4xl lg:text-6xl font-extrabold text-center">
        Hubungi Kami
        </h1>
      </section>

      {/* Contact Info & Form Section */}
      <div className="container mx-auto py-16 px-6 lg:px-16">
        <div className="grid md:grid-cols-2 gap-12">
          {/* Contact Info */}
          <div>
            <h2 className="text-4xl font-extrabold text-gray-900">Hubungi Kami</h2>
            <p className="mt-4 text-lg text-gray-600 leading-relaxed">
              Jika Anda memiliki pertanyaan atau ingin berbicara langsung dengan tim kami, silakan hubungi kami melalui informasi di bawah ini.
            </p>

            {/* Contact Details */}
            <div className="mt-8 space-y-6">
              <div className="flex items-center space-x-4 p-4 bg-gray-100 rounded-lg shadow-md">
                <FaMapMarkerAlt className="text-primary-600 text-3xl" />
                <p className="text-gray-700">
                  Jl. Rawa lele No. 157G RT 008/010, Kalideres, Jakarta Barat 11840
                </p>
              </div>
              <div className="flex items-center space-x-4 p-4 bg-gray-100 rounded-lg shadow-md">
                <FaEnvelope className="text-primary-600 text-3xl" />
                <p className="text-gray-700">support@daridanke.com</p>
              </div>
              <div className="flex items-center space-x-4 p-4 bg-gray-100 rounded-lg shadow-md">
                <FaPhoneAlt className="text-primary-600 text-3xl" />
                <p className="text-gray-700">+62 812-3456-7890</p>
              </div>
            </div>
          </div>

          {/* Contact Form */}
          <div className="bg-white p-10 shadow-2xl rounded-2xl border border-gray-200">
            <h2 className="text-3xl font-extrabold text-gray-900 text-center">Kirim Pesan</h2>
            <form action="#" className="mt-6 space-y-6">
              <div>
                <label htmlFor="name" className="block text-sm font-semibold text-gray-900">
                  Nama Anda
                </label>
                <input
                  type="text"
                  id="name"
                  className="mt-2 block w-full p-3 border border-gray-300 rounded-lg shadow-sm focus:ring-primary-500 focus:border-primary-500 transition-all duration-200"
                  placeholder="Masukkan nama Anda"
                  required
                />
              </div>

              <div>
                <label htmlFor="email" className="block text-sm font-semibold text-gray-900">
                  Email Anda
                </label>
                <input
                  type="email"
                  id="email"
                  className="mt-2 block w-full p-3 border border-gray-300 rounded-lg shadow-sm focus:ring-primary-500 focus:border-primary-500 transition-all duration-200"
                  placeholder="name@example.com"
                  required
                />
              </div>

              <div>
                <label htmlFor="subject" className="block text-sm font-semibold text-gray-900">
                  Subjek
                </label>
                <input
                  type="text"
                  id="subject"
                  className="mt-2 block w-full p-3 border border-gray-300 rounded-lg shadow-sm focus:ring-primary-500 focus:border-primary-500 transition-all duration-200"
                  placeholder="Tulis subjek pesan Anda"
                  required
                />
              </div>

              <div>
                <label htmlFor="message" className="block text-sm font-semibold text-gray-900">
                  Pesan Anda
                </label>
                <textarea
                  id="message"
                  rows="5"
                  className="mt-2 block w-full p-3 border border-gray-300 rounded-lg shadow-sm focus:ring-primary-500 focus:border-primary-500 transition-all duration-200"
                  placeholder="Tulis pesan Anda di sini..."
                  required
                ></textarea>
              </div>

              <div className="flex justify-end">
                <button
                  type="submit"
                  className="py-3 px-5 text-sm font-semibold text-center rounded-lg bg-primary-700 border border-gray-300 hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 transition-all duration-200"
                >
                  Kirim Pesan
                </button>
              </div>

            </form>
          </div>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default ContactUs;

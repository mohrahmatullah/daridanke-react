import React, { useEffect, useState } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import Sidebar from '../components/Sidebar';
import { getProvincies, getCities, getDistrict, getSubDistrict, getPostalCode, createAddress, geAddressByUserIdAddress } from '../services/address';
import { Link, useNavigate } from "react-router-dom";
import jwt_decode from "jwt-decode";

const CreateAddress = () => {
    const navigate = useNavigate();
    const [selectNegara, setSelectNegara] = useState('');
    const [selectedProvinsi, setSelectedProvinsi] = useState('');
    const [selectedKabupaten, setSelectedKabupaten] = useState('');
    const [selectedKecamatan, setSelectedKecamatan] = useState('');
    const [selectedKelurahan, setSelectedKelurahan] = useState('');
    const [selectedPostalCode, setSelectedPostalCode] = useState('');
    const [textareaAddress, setTextareaAddress] = useState([]);
    const [inputNoHP, setInputNoHP] = useState([]);
    const [provinsiData, setProvinsiData] = useState([]);
    const [kabupatenData, setKabupatenData] = useState([]);
    const [kecamatanData, setKecamatanData] = useState([]);
    const [kelurahanData, setKelurahanData] = useState([]);
    const [postalCodeData, setPostalCodeData] = useState([]);
    const token = localStorage.getItem('token');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');

    useEffect(() => {
        if (!token) {
          navigate('/login');
        }
        fetchProvinsiData();
        viewAddress();
    }, []);

    const viewAddress = async () => {
      
      try {  
        if (token) {
          // const decode = jwt_decode(token);
          // let data = {
          //   id_user       : decode.id
          // }
          // const response = await geAddressByUserIdAddress(token, data);
          // console.log(response.data)
          
          // setSelectedProvinsi(response.data.Prov_id)
          // setSelectedKabupaten(response.data.City_id)
          // setSelectedKecamatan(response.data.Dis_id)
          // setSelectedKelurahan(response.data.Subdis_id)
          // setPostalCodeData(response.data.Postal_code)
          // setTextareaAddress(response.data.Address)
          // setInputNoHP(response.data.No_telp)
        }     

      } catch (error) {
        console.error('Error fetching provinsi data:', error);
      }
    };

    const fetchProvinsiData = async () => {
      
      try {
        const response = await getProvincies(token);

        setProvinsiData(response.data); // Assuming the response is an array of provinces
      } catch (error) {
        console.error('Error fetching provinsi data:', error);
      }
    };

    const handleProvinsiChange = async (event) => {
      const value = event.target.value;
      setSelectedProvinsi(value);

      try {
        if(value){
          // Fetch data based on the selected province
          const response = await getCities(token, value);  
          // Assuming your data structure has an array of regencies/cities
          setKabupatenData(response.data);
          setKecamatanData('');
          setKelurahanData('');
          setPostalCodeData('');
        }
        else{
          setKabupatenData('');
          setKecamatanData('');
          setKelurahanData('');
          setPostalCodeData('');
        }
      } catch (error) {
        console.error('Error fetching data based on province:', error);
      }
    };

    const handleKabupatenChange = async (event) => {
      const value = event.target.value;
      setSelectedKabupaten(value);

      try {
        // Fetch data based on the selected regency/city
        if(value){
          const response = await getDistrict(token, value);

          // Assuming your data structure has an array of districts
          setKecamatanData(response.data);
          setKelurahanData('');
          setPostalCodeData('');
        }
        else{
          setKecamatanData('');
          setKelurahanData('');
          setPostalCodeData('');
        }
      } catch (error) {
        console.error('Error fetching data based on regency/city:', error);
      }
    };

    const handleKecamatanChange = async (event) => {
      const value = event.target.value;
      setSelectedKecamatan(value);

      try {
        if(value){
        // Fetch data based on the selected district
          const response = await getSubDistrict(token, value);

          setKelurahanData(response.data);
          setPostalCodeData('');
        }
        else{
          setKelurahanData('');
          setPostalCodeData('');
        }
      } catch (error) {
        console.error('Error fetching data based on district:', error);
      }
    };

    const handleKelurahanChange = async (event) => {
      const value = event.target.value;
      setSelectedKelurahan(value);
      // Add your logic here for handling the selected village
      try {
        if(value){
        // Fetch data based on the selected district
          const response = await getPostalCode(token, value);

          setPostalCodeData(response.data.PostalCode);
        }
        else{
          setPostalCodeData('');
        }
      } catch (error) {
        console.error('Error fetching data based on district:', error);
      }
    };

    const handlePostalCodeChange = async (event) => {
      const value = event.target.value;
      setSelectedPostalCode(value);
    };

    const handleSubmit = async (event) => {
      // Add your logic here for submitting the form
      const decode = jwt_decode(token);
      let data = {
        id_user       : decode.id,
        negara        : selectNegara ,
        prov_id       : Number(selectedProvinsi),
        city_id       : Number(selectedKabupaten),
        dis_id        : Number(selectedKecamatan),
        subdis_id     : Number(selectedKelurahan),
        postal_code   : Number(postalCodeData),
        address       : textareaAddress,
        no_telp       : inputNoHP
      }
      
      await createAddress(token, data)

      setAlertMessage('Success');
      setShowAlert(true);
      setTimeout(() => {
          setShowAlert(false);
          setAlertMessage('');
          navigate('/address');
      }, 1000);
      console.log(data)
    };

    return (
        <>
        <Header />
          <div className="container py-4 flex items-center gap-3">
              <a href="../index.html" className="text-primary text-base">
                  <i className="fa-solid fa-house"></i>
              </a>
              <span className="text-sm text-gray-400">
                  <i className="fa-solid fa-chevron-right"></i>
              </span>
              <p className="text-gray-600 font-medium">Address</p>
          </div>

          <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">

              <Sidebar />

              <div className="col-span-9 shadow rounded px-6 pt-5 pb-7">
                <h4 className="text-lg font-medium capitalize mb-4">
                  Manage Address
                </h4>
                <div className="space-y-4">
                  <div className="grid grid-cols-2 gap-4">
                    <div>
                      <label htmlFor="negara">Negara</label>
                      <select name="negara" id="negara" className="input-box" onChange={(e) => setSelectNegara(e.target.value)} value={selectNegara}>
                        <option value="indonesia">Indonesia</option>
                      </select>
                    </div>
                    <div>
                      <label htmlFor="provinsi">Provinsi</label>
                      <select value={selectedProvinsi} name="provinsi" id="provinsi" className="input-box" onChange={handleProvinsiChange}>
                        <option value="">- Pilih Provinsi -</option>
                        {provinsiData && (
                          provinsiData.map((provinsi) => (
                            <option key={provinsi.ProvID} value={provinsi.ProvID}>
                              {provinsi.ProvName}
                            </option>
                          ))
                        )}
                      </select>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div>
                      <label htmlFor="kota_kabupaten">Kota / Kabupaten</label>
                      <select value={selectedKabupaten} name="kota_kabupaten" id="kota_kabupaten" className="input-box" onChange={handleKabupatenChange}>
                        <option value="">- Pilih Kota / Kabupaten -</option>
                        {kabupatenData && (
                          kabupatenData.map((kabupaten) => (
                            <option key={kabupaten.CityId} value={kabupaten.CityId}>
                              {kabupaten.CityName}
                            </option>
                          ))
                        )}
                      </select>
                    </div>
                    <div>
                      <label htmlFor="kecamatan">Kecamatan</label>
                      <select value={selectedKecamatan} name="kecamatan" id="kecamatan" className="input-box" onChange={handleKecamatanChange}>
                        <option value="">- Pilih Kecamatan -</option>
                        {kecamatanData && (
                          kecamatanData.map((kecamatan) => (
                            <option key={kecamatan.DisId} value={kecamatan.DisId}>
                              {kecamatan.DisName}
                            </option>
                          ))
                        )}
                      </select>
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div>
                      <label htmlFor="kelurahan">Kelurahan</label>
                      <select value={selectedKelurahan} name="kelurahan" id="kelurahan" className="input-box" onChange={handleKelurahanChange}>
                        <option value="">- Pilih Kelurahan -</option>
                        {kelurahanData && (
                          kelurahanData.map((kelurahan) => (
                            <option key={kelurahan.SubdisId} value={kelurahan.SubdisId}>
                              {kelurahan.SubdisName}
                            </option>
                          ))
                        )}
                      </select>
                    </div>
                    <div>
                      <label htmlFor="kode_pos">Kode Pos</label>
                      <input type="text" name="kode_pos" id="kode_pos" className="input-box" onChange={handlePostalCodeChange} value={postalCodeData} />
                      {/* <select value={selectedPostalCode} name="kode_pos" id="kode_pos" className="input-box" onChange={handlePostalCodeChange}>
                        {postalCodeData && (
                          postalCodeData.map((postalCode) => (
                            <option key={postalCode.PostalId} value={postalCode.PostalId}>
                              {postalCode.PostalCode}
                            </option>
                          ))
                        )}
                      </select> */}
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div>
                      <label htmlFor="address">Address</label>
                      <textarea type="text" name="address" id="address" className="input-box" onChange={(e) => setTextareaAddress(e.target.value)} value={textareaAddress} />
                    </div>
                  </div>
                  <div className="grid grid-cols-2 gap-4">
                    <div>
                      <label htmlFor="address">No Handphone</label>
                      <input type="text" name="no_hp" id="no_hp" className="input-box" onChange={(e) => setInputNoHP(e.target.value)} value={inputNoHP} />
                    </div>
                  </div>
                </div>

                <div className="mt-4">
                    <button type="submit" onClick={handleSubmit} className="py-3 px-4 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">save changes</button>
                </div>
              </div>

          </div>
          {showAlert && (
            <div className="bg-red-500 text-white py-3 px-4 rounded-md absolute top-3 right-3">
                {alertMessage}
            </div>
          )}
        <Footer />
        </>
    );
}

export default CreateAddress
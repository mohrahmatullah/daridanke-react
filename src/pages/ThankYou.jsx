import React from 'react';
import { Link, useParams } from "react-router-dom";
import Header from '../components/Header';
import Footer from '../components/Footer';

const ThankYou = () => {
  const { id } = useParams();

  // Nomor rekening untuk konfirmasi pembayaran
  const bcaAccountNumber = "123-456-7890";
  const accountHolderName = "Your Name";

  return (
    <>
      <Header />
      <div className="container mx-auto mt-16 text-center">
        <h1 className="text-4xl font-bold mb-4 text-primary">Thank You!</h1>
        <p className="text-lg mb-8">
          Terima kasih telah melakukan pembelian. Pesanan Anda sedang diproses.
        </p>
        <p className="text-lg mb-8">
          Silahkan lakukan pembayaran ke nomor rekening berikut:
          <br />
          <span className="font-bold">Bank BCA:</span> {bcaAccountNumber}
          <br />
          <span className="font-bold">Atas Nama:</span> {accountHolderName}
        </p>
        <div className="flex justify-center space-x-4">
          <Link to={`/payment-confirmation/${id}`}>
            <button className="py-3 px-6 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">
              Konfirmasi Pembayaran
            </button>
          </Link>
          <Link to="/shop">
            <button className="py-3 px-6 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">
              Belanja Lagi
            </button>
          </Link>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default ThankYou;

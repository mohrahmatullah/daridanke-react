import React, { useState, useEffect } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import { formatDate } from '../helpers/helpers'; 
import avatar from './../../public/assets/images/avatar.png'
import product5 from './../../public/assets/images/products/product5.jpg'
import product6 from './../../public/assets/images/products/product6.jpg'
import product10 from './../../public/assets/images/products/product10.jpg'
import Sidebar from '../components/Sidebar';
import { getOrder } from '../services/order';
import { Link, useNavigate, useParams } from "react-router-dom";
import jwt_decode from "jwt-decode";

const Order = () => {
    const navigate = useNavigate();    
    const { search } = useParams();
    const [orders, setOrders] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    
    const token = localStorage.getItem('token');
    useEffect(() => {
        if (!token) {
            navigate('/login');
        }
        const searchParam = search || '';
        fetchOrderData(currentPage, searchParam, token);
    }, [currentPage, search, token ]);

    const fetchOrderData = async (page, search, token) => {
        try {
            if (token) {
                const decode = jwt_decode(token);
                const response = await getOrder(page, search, decode, token);
                setOrders(response.data.posts);
                setCurrentPage(response.data.currentPage);
                setTotalPages(response.data.totalPages);
                console.log(response);
            }
        } catch (error) {
            console.error('Error placing order:', error);
        } 
    };

    const handlePagination = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    return (
        <>
        <Header />
            <div className="container py-4 flex items-center gap-3">
                <a href="../index.html" className="text-primary text-base">
                    <i className="fa-solid fa-house"></i>
                </a>
                <span className="text-sm text-gray-400">
                    <i className="fa-solid fa-chevron-right"></i>
                </span>
                <p className="text-gray-600 font-medium">Order</p>
            </div>
            <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">

                <Sidebar />

                <div className="col-span-9 space-y-4">
                    

                    {orders.length === 0 ? (
                        <div className="flex items-center justify-center h-screen">
                            <div className="text-center">
                                <h2 className="text-3xl font-semibold mb-4">Your Order is Empty</h2>
                                <p className="text-gray-500 mb-8">Looks like you haven't added any items to your cart yet.</p>
                                <Link to="/shop">
                                    <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Shop Now</button>
                                </Link>                            
                            </div>
                        </div>
                    ) : (
                        <>
                            {orders.map(orders => (                                                         
                                <Link to={`/invoice/${orders.ID}`} key={orders.ID} className="flex items-center justify-between border gap-6 p-4 border-gray-200 rounded">
                                    {/* <div className="w-28">
                                        <img src={product6} alt="product 6" className="w-full" />
                                    </div> */}
                                    <div className="w-1/3">
                                        <h2 className="text-gray-800 text-xl font-medium uppercase">{orders.Inv_no}</h2>
                                        <p className="text-gray-500 text-sm">Date Order: <span className="text-green-600">{formatDate(orders.CreatedAt)}</span></p>
                                    </div>
                                    {/* <div className="text-primary text-lg font-semibold">$320.00</div> */}
                                    <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">{orders.Post_status}</button>

                                    {/* <div className="text-gray-600 cursor-pointer hover:text-primary">
                                        <i className="fa-solid fa-trash"></i>
                                    </div> */}
                                </Link>
                            ))}
                        </>          
                    )}

                    
                    {/* Pagination */}
                    {totalPages > 1 && (
                        <div className="flex justify-center mt-4">
                        <ul className="flex space-x-2">
                            {[...Array(totalPages).keys()].map((pageNumber) => (
                            <li key={pageNumber}>
                                <button
                                className={`px-3 py-1 rounded-md ${
                                    pageNumber + 1 === currentPage ? 'bg-gray-600 text-white' : 'bg-gray-200'
                                }`}
                                onClick={() => handlePagination(pageNumber + 1)}
                                >   
                                {pageNumber + 1}
                                </button>
                            </li>
                            ))}
                        </ul>
                        </div>
                    )}
                </div>


            </div>
        <Footer />
        </>
    );
}

export default Order
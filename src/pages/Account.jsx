import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Sidebar from "../components/Sidebar";
import { Link, useNavigate } from "react-router-dom";
import { geAddressByUserIdAddress } from "../services/address";
import jwt_decode from "jwt-decode";

const Account = () => {
  const navigate = useNavigate();
  const token = localStorage.getItem("token");
  const [address, setAddress] = useState("");
  useEffect(() => {
    document.title = "Daridanke | Profile Information";
    if (!token) {
      navigate("/login");
    }
    viewAddress();
  }, []);
  const viewAddress = async () => {
    try {
      if (token) {
        const decode = jwt_decode(token);
        let data = {
          id_user: decode.id,
        };
        const response = await geAddressByUserIdAddress(token, data);
        setAddress(response.data);
        console.log(response.data);
      }
    } catch (error) {
      console.error("Error fetching provinsi data:", error);
    }
  };
  return (
    <>
      <Header />

      <div className="container py-4 flex items-center gap-3">
        <a href="../index.html" className="text-primary text-base">
          <i className="fa-solid fa-house"></i>
        </a>
        <span className="text-sm text-gray-400">
          <i className="fa-solid fa-chevron-right"></i>
        </span>
        <p className="text-gray-600 font-medium">Account</p>
      </div>

      <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">
        <Sidebar />

        <div className="col-span-9 grid grid-cols-2 gap-4">
          <div className="shadow rounded bg-white px-4 pt-6 pb-8">
            <div className="flex items-center justify-between mb-4">
              <h3 className="font-medium text-gray-800 text-lg">
                Personal Profile
              </h3>
              <Link to="/profile" className="text-primary">
                Edit
              </Link>
            </div>
            <div className="space-y-1">
              {token ? (
                <>
                  <h4 className="text-gray-700 font-medium">
                    {jwt_decode(token).username}
                  </h4>
                  <p className="text-gray-800">{jwt_decode(token).email}</p>
                  <p className="text-gray-800">0811 8877 988</p>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>

          <div className="shadow rounded bg-white px-4 pt-6 pb-8">
            <div className="flex items-center justify-between mb-4">
              <h3 className="font-medium text-gray-800 text-lg">
                Shipping address
              </h3>
              <Link to="/address" className="text-primary">
                Edit
              </Link>
            </div>
            <div className="space-y-1">
              {token && address ? (
                <>
                  <h4 className="text-gray-700 font-medium">
                    {jwt_decode(token).username}
                  </h4>
                  <p className="text-gray-800">
                    {address.Prov.ProvName}, {address.City.CityName},{" "}
                  </p>
                  <p className="text-gray-800">
                    {address.Dis.DisName}, {address.Subdis.SubdisName}
                  </p>
                  <p className="text-gray-800">{address.Postal_code}</p>
                  <p className="text-gray-800">{address.No_telp}</p>
                </>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default Account;

import React, { useEffect, useState } from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import deliveryvan from "./../../public/assets/images/icons/delivery-van.svg";
import moneyback from "./../../public/assets/images/icons/money-back.svg";
import servicehours from "./../../public/assets/images/icons/service-hours.svg";
import offer from "./../../public/assets/images/offer.jpg";
import { getCategory, getTopShop, getRekomendasiShop } from "../services/shop";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addToCart } from "../redux/actions/actions";
import { formatCurrency } from "../helpers/helpers";
import { useNavigate } from "react-router-dom"; 

const Home = () => {
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState(null);
  const [dataTop, setDataTop] = useState(null);
  const [dataRekomendasi, setDataRekomendasi] = useState(null);
  const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;
  const [quantity, setQuantity] = useState(1);
  const [showAlert, setShowAlert] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    document.title = "Daridanke | Home";
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await getCategory();
      setData(response.data);

      const responseTop = await getTopShop();
      setDataTop(responseTop.data.products);

      const responseRekomendasi = await getRekomendasiShop();
      setDataRekomendasi(responseRekomendasi.data.products);
    } catch (error) {
      console.error("Error fetching product:", error);
    }
  };

  const handleAddToCart = (product, quantity) => {
    const existingCartItems =
      JSON.parse(localStorage.getItem("cartItems")) || [];
    const existingProductIndex = existingCartItems.findIndex(
      (item) => item.ID === product.ID
    );

    if (existingProductIndex !== -1) {
      // If the product is already in the cart, update its quantity and total price
      existingCartItems[existingProductIndex].quantity += quantity;
      existingCartItems[existingProductIndex].totalPrice =
        existingCartItems[existingProductIndex].Price *
        existingCartItems[existingProductIndex].quantity;
    } else {
      dispatch(addToCart(product));
      // If the product is not in the cart, add it with the total price calculated based on quantity
      const productWithQuantity = {
        ...product,
        quantity,
        totalPrice: product.Price * quantity,
      };
      existingCartItems.push(productWithQuantity);

      setShowAlert(true);
      setTimeout(() => {
        setShowAlert(false);
      }, 500);
    }

    localStorage.setItem("cartItems", JSON.stringify(existingCartItems));
  };

  return (
    <>
      <Header />

      {/* <div className="bg-cover bg-no-repeat bg-center py-36 bg-body-slider">
        <div className="container">
          <h1 className="text-6xl text-gray-800 font-medium mb-4 capitalize">
            best collection for <br /> home decoration
          </h1>
          <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aperiam{" "}
            <br />
            accusantium perspiciatis, sapiente magni eos dolorum ex quos dolores
            odio
          </p>
          <div className="mt-12">
            <Link
              to="/shop"
              className="bg-primary border border-primary text-white px-8 py-3 font-medium rounded-md hover:bg-transparent hover:text-primary w-10"
            >
              Shop Now
            </Link>
          </div>
        </div>
      </div> */}

      <div className="relative bg-cover bg-no-repeat bg-center py-36 bg-body-slider">
        <div className="absolute inset-0 bg-black/50 backdrop-blur-sm"></div>
        <div className="container relative z-10 text-center text-white">
          <h1 className="text-5xl md:text-6xl font-semibold mb-6 leading-tight">
            Digital Products for <br /> Your Business Growth
          </h1>
          <p className="text-lg max-w-2xl mx-auto text-gray-200">
            From legal documents to business proposals, get professionally designed
            templates & forms to streamline your workflow effortlessly.
          </p>
          <div className="mt-12">
            <Link
              to="/shop"
              className="bg-primary border border-primary text-white px-8 py-3 font-medium rounded-lg shadow-md hover:bg-transparent hover:text-primary transition-all"
            >
              Explore Now
            </Link>
          </div>
        </div>
      </div>


      <div className="container py-16">
        <div className="w-10/12 grid grid-cols-1 md:grid-cols-3 gap-8 mx-auto">
          {[
            {
              img: deliveryvan,
              title: "Free Shipping",
              desc: "Order over $200",
            },
            {
              img: moneyback,
              title: "Money Returns",
              desc: "30 days money returns",
            },
            {
              img: servicehours,
              title: "24/7 Support",
              desc: "Customer support",
            },
          ].map((item, index) => (
            <div
              key={index}
              className="border border-gray-200 shadow-lg rounded-lg p-6 flex items-center gap-6 bg-white transition-all hover:scale-105 hover:shadow-xl"
            >
              <div className="w-16 h-16 bg-primary/10 flex items-center justify-center rounded-full">
                <img src={item.img} alt={item.title} className="w-10 h-10 object-contain" />
              </div>
              <div>
                <h4 className="font-semibold text-lg text-gray-900">{item.title}</h4>
                <p className="text-gray-500 text-sm">{item.desc}</p>
              </div>
            </div>
          ))}
        </div>
      </div>

      <div className="container py-16">
        <h2 className="text-3xl font-semibold text-gray-900 uppercase mb-8 text-center">
          Shop by Category
        </h2>
        <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-6">
          {data &&
            data.map((item, index) => (
              <div
                key={index}
                className="relative group rounded-xl overflow-hidden shadow-lg hover:shadow-2xl transition-all duration-300"
              >
                {/* Gambar tetap tampil penuh */}
                <img
                  src={`${apiUrl}/files/${item.Image}`}
                  alt={item.Title}
                  className="w-full h-56 object-cover transition-transform duration-300 group-hover:scale-105"
                />
                {/* Overlay dengan efek hover */}
                <div className="absolute inset-0 bg-gray-700 bg-opacity-30 group-hover:bg-opacity-50 transition-all duration-300"></div>
                {/* Nama kategori selalu tampil */}
                <div
                  href="#"
                  className="absolute inset-0 flex items-center justify-center text-lg md:text-xl text-white font-semibold tracking-wide"
                >
                  {item.Title}
                </div>
              </div>
            ))}
        </div>
      </div>



      <div className="container pb-16">
        <h2 className="text-2xl font-medium text-gray-800 uppercase mb-6">
          top new arrival
        </h2>
        <div className="grid md:grid-cols-4 sm:grid-cols-2 grid-cols-1 gap-6">
          {dataTop &&
            dataTop !== null &&
            dataTop.map((product) => (
            <Link
              to={`/product/${product.ID}`}
              key={product.ID}
              className="group relative block bg-white rounded-2xl overflow-hidden shadow-md hover:shadow-xl transition-shadow"
            >
              <div className="relative w-full h-50">
                <img
                  src={`${apiUrl}/files/${product.Image}`}
                  alt={product.Name}
                  className="w-full h-full object-cover object-center transition-transform duration-300 group-hover:scale-105"
                />
                <div className="absolute inset-0 bg-black bg-opacity-20 group-hover:bg-opacity-40 transition-opacity flex items-center justify-center gap-3 opacity-0 group-hover:opacity-100">
                  <div
                    className="w-10 h-10 bg-primary text-white rounded-full flex items-center justify-center cursor-pointer hover:bg-gray-800 transition"
                    title="View product"
                  >
                    <i className="fa-solid fa-magnifying-glass"></i>
                  </div>
                  <button
                    onClick={() => navigate("/wishlist")}
                    className="w-10 h-10 bg-primary text-white rounded-full flex items-center justify-center hover:bg-gray-800 transition"
                    title="Add to wishlist"
                  >
                    <i className="fa-solid fa-heart"></i>
                  </button>
                </div>
              </div>

              <div className="p-5 space-y-1 mb-10">
                <h4 className="text-lg font-semibold text-gray-800 group-hover:text-primary transition">
                  {product.Name}
                </h4>
                <div className="flex items-center space-x-2">
                  <p className="text-lg font-bold text-primary">
                    {formatCurrency(product.Price)}
                  </p>
                </div>
              </div>

              <button
               onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  handleAddToCart(product, quantity);
                }}
                className="absolute bottom-0 left-0 w-full py-4 text-white bg-primary rounded-b-2xl text-sm font-semibold tracking-wide hover:bg-transparent hover:text-primary border border-primary transition"
              >
                Add to Cart
              </button>
            </Link>
          ))}
        </div>
      </div>

      <div className="container pb-16">
        <div href="#">
          <img src={offer} alt="ads" className="w-full" />
        </div>
      </div>

      <div className="container pb-16">
        <h2 className="text-2xl font-medium text-gray-800 uppercase mb-6">
          recomended for you
        </h2>
        <div className="grid md:grid-cols-4 sm:grid-cols-2 grid-cols-1 gap-6">
          {dataRekomendasi &&
            dataRekomendasi !== null &&
            dataRekomendasi.map((product) => (
            <Link
              to={`/product/${product.ID}`}
              key={product.ID}
              className="group relative block bg-white rounded-2xl overflow-hidden shadow-md hover:shadow-xl transition-shadow"
            >
              <div className="relative w-full h-50">
                <img
                  src={`${apiUrl}/files/${product.Image}`}
                  alt={product.Name}
                  className="w-full h-full object-cover object-center transition-transform duration-300 group-hover:scale-105"
                />
                <div className="absolute inset-0 bg-black bg-opacity-20 group-hover:bg-opacity-40 transition-opacity flex items-center justify-center gap-3 opacity-0 group-hover:opacity-100">
                  <div
                    className="w-10 h-10 bg-primary text-white rounded-full flex items-center justify-center cursor-pointer hover:bg-gray-800 transition"
                    title="View product"
                  >
                    <i className="fa-solid fa-magnifying-glass"></i>
                  </div>
                  <button
                    onClick={() => navigate("/wishlist")}
                    className="w-10 h-10 bg-primary text-white rounded-full flex items-center justify-center hover:bg-gray-800 transition"
                    title="Add to wishlist"
                  >
                    <i className="fa-solid fa-heart"></i>
                  </button>
                </div>
              </div>

              <div className="p-5 space-y-1 mb-10">
                <h4 className="text-lg font-semibold text-gray-800 group-hover:text-primary transition">
                  {product.Name}
                </h4>
                <div className="flex items-center space-x-2">
                  <p className="text-lg font-bold text-primary">
                    {formatCurrency(product.Price)}
                  </p>
                </div>
              </div>

              <button
               onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  handleAddToCart(product, quantity);
                }}
                className="absolute bottom-0 left-0 w-full py-4 text-white bg-primary rounded-b-2xl text-sm font-semibold tracking-wide hover:bg-transparent hover:text-primary border border-primary transition"
              >
                Add to Cart
              </button>
            </Link>
          ))}
        </div>
      </div>

      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-3xl font-semibold">Modal Title</h3>
                  <button
                    className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <p className="my-4 text-blueGray-500 text-lg leading-relaxed">
                    I always felt like I could do anything. That’s the main
                    thing people are controlled by! Thoughts- their perception
                    of themselves! They're slowed down by their perception of
                    themselves. If you're taught you can’t do anything, you
                    won’t do anything. I was taught I could do everything.
                  </p>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                  <button
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    Save Changes
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}

      {showAlert && (
        <div className="bg-green-500 text-white py-3 px-4 rounded-md absolute top-5 right-3">
          Item added to cart successfully!
        </div>
      )}
      <Footer />
    </>
  );
};

export default Home;

import React, { useEffect } from "react";

const MetaTags = ({ ogData, tagOgData, otherMeta }) => {
  useEffect(() => {
    if (ogData) {
      const fi = ogData.cover.split(".");
      const ogImage = `${process.env.REACT_APP_URL_MEDIA}${fi[0]}_600x315.${fi[1]}`;
      const ogImageSecure = ogImage.replace("http:", "https:");

      document.title = ogData.title;
      setMetaTag("og:title", ogData.title);
      setMetaTag("og:description", ogData.seo_description);
      setMetaTag("og:image", ogImage);
      setMetaTag("og:site_name", "daridanke");
      setMetaTag("og:image:secure_url", ogImageSecure);
      setMetaTag("description", ogData.seo_description);
      setMetaTag("keywords", ogData.seo_keywords);
    }

    if (tagOgData) {
      const description = `Berikut ini kumpulan dan informasi berita terbaru mengenai ${tagOgData.name} yang diulas lengkap dan valid oleh daridanke.com yang bisa Anda baca setiap harinya!`;
      document.title = `Kumpulan Berita Harian ${tagOgData.name} Terbaru`;
      setMetaTag(
        "og:title",
        `Kumpulan Berita Harian ${tagOgData.name} Terbaru`
      );
      setMetaTag("og:description", description);
      setMetaTag("og:site_name", "daridanke.com");
      setMetaTag("description", description);
      setMetaTag(
        "keywords",
        `Berita ${tagOgData.name}, Info ${tagOgData.name}, Foto ${tagOgData.name}`
      );
    }

    // Set other meta tags
    setMetaTag("og:type", "website");
    setMetaTag("og:locale", "en_ID");
    setMetaTag("og:url", window.location.href);
    setMetaTag("og:updated_time", new Date().toISOString());

    // Other specific meta tags
    if (otherMeta) {
      otherMeta.forEach(({ name, content }) => {
        setMetaTag(name, content);
      });
    }
  }, [ogData, tagOgData, otherMeta]);

  const setMetaTag = (name, content) => {
    let tag = document.querySelector(
      `meta[name='${name}'], meta[property='${name}']`
    );
    if (!tag) {
      tag = document.createElement("meta");
      if (name.startsWith("og:") || name.startsWith("fb:")) {
        tag.setAttribute("property", name);
      } else {
        tag.setAttribute("name", name);
      }
      document.head.appendChild(tag);
    }
    tag.setAttribute("content", content);
  };

  return null;
};

export default MetaTags;

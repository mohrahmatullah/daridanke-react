import React, { useEffect } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import avatar from './../../public/assets/images/avatar.png'
import product5 from './../../public/assets/images/products/product5.jpg'
import product6 from './../../public/assets/images/products/product6.jpg'
import product10 from './../../public/assets/images/products/product10.jpg'
import Sidebar from '../components/Sidebar';
import { Link, useNavigate } from "react-router-dom";

const Voucher = () => {
    const navigate = useNavigate();
    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
            navigate('/login');
        }
    }, []);

    return (
        <>
        <Header />
            <div className="container py-4 flex items-center gap-3">
                <a href="../index.html" className="text-primary text-base">
                    <i className="fa-solid fa-house"></i>
                </a>
                <span className="text-sm text-gray-400">
                    <i className="fa-solid fa-chevron-right"></i>
                </span>
                <p className="text-gray-600 font-medium">Wishlist</p>
            </div>
            <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">

                <Sidebar />

                <div className="col-span-9 space-y-4">
                    <div className="flex items-center justify-center h-screen">
                        <div className="text-center">
                            <h2 className="text-3xl font-semibold mb-4">Comming Soon</h2>
                            <Link to="/shop">
                                <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Shop Now</button>
                            </Link>                            
                        </div>
                    </div>
                </div>

            </div>
        <Footer />
        </>
    );
}

export default Voucher
import React, { useState, useEffect } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import { formatDate, formatCurrency } from '../helpers/helpers';
import avatar from './../../public/assets/images/avatar.png'
import product5 from './../../public/assets/images/products/product5.jpg'
import product6 from './../../public/assets/images/products/product6.jpg'
import product10 from './../../public/assets/images/products/product10.jpg'
import Sidebar from '../components/Sidebar';
import { getOrderByID } from '../services/order';
import { Link, useNavigate, useParams } from "react-router-dom";
import { getPaymentConfirmationByID } from '../services/payment_confirmations';


const Invoice = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const [orderData, setOrderData] = useState(null);
  const [showAlert, setShowAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [buktiPembayaran, setBuktiPembayaran] = useState(null);
  const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;

  const token = localStorage.getItem('token');
  useEffect(() => {
    if (!token) {
      navigate('/login');
    }
    fetchOrderData(id, token);
  }, [id, token]);

  const fetchOrderData = async (id, token) => {
    try {
      if (token) {
        const response = await getOrderByID(id, token);
        setOrderData(response.data);
        const result = await getPaymentConfirmationByID(id, token);
        setBuktiPembayaran(result.data)
      }
    } catch (error) {
      console.error('Error placing order:', error);
    }
  };

  const totalPrice = orderData && orderData.Order_data ? orderData.Order_data.reduce((total, item) => total + item.Price, 0) : 0;

  const fetchLihatConfirmation = async (id, token) => {
    try {
      const response = await getPaymentConfirmationByID(id, token);
      setShowModal(true)
      setBuktiPembayaran(response.data)
      console.log(response);
    } catch (error) {
      console.error('Error fetching login data:', error);
    }
  };

  const handleLihatConfirmation = () => {

    try {
      if (token) {
        fetchLihatConfirmation(id, token);
      } else {
        setAlertMessage('Silahkan Login Terlebih Dahulu');
        setShowAlert(true);
        setTimeout(() => {
          setShowAlert(false);
          setAlertMessage('');
        }, 2000);
      }
    } catch (error) {
      console.error('Error placing order:', error);
    }
  };

  return (
    <>
      <Header />
      <div className="container py-4 flex items-center gap-3">
        <a href="../index.html" className="text-primary text-base">
          <i className="fa-solid fa-house"></i>
        </a>
        <span className="text-sm text-gray-400">
          <i className="fa-solid fa-chevron-right"></i>
        </span>
        <p className="text-gray-600 font-medium">Invoice</p>
      </div>
      <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">

        <Sidebar />

        <div className="col-span-9 space-y-4">
          {orderData !== null && (
            <div className="bg-white p-8 border rounded-lg shadow-lg">
              <div className="flex items-center justify-between mb-6">
                <h2 className="text-3xl font-semibold">Invoice #{orderData.Inv_no}</h2>
                <p className="text-gray-500">{formatDate(orderData.CreatedAt)}</p>
              </div>

              <div className="mb-8">
                <h2 className="text-lg font-bold mb-4">Address:</h2>
                <div className="text-gray-700 mb-2">{orderData.Address_customer}</div>
              </div>
              <div className="w-full mb-8">
                <div className="flex justify-between border-b pb-2">
                  <div className="text-left font-bold text-gray-700">Item</div>
                  <div className="text-right font-bold text-gray-700">Amount</div>
                </div>
                {orderData.Order_data.map((item, index) => (
                  <div key={index} className="flex items-center justify-between border-b py-4">
                    <div className="flex items-center">
                      <img src={`${apiUrl}/files/${item.Image}`} alt={item.Name} className="w-12 h-12 object-cover mr-4" />
                      <div>
                        <p className="text-lg font-semibold">{item.Name}</p>
                        <p className="text-gray-500">{formatDate(item.CreatedAt)}</p>
                      </div>
                    </div>
                    <p className="text-lg">{formatCurrency(item.Price)}</p>
                  </div>
                ))}
              </div>

              <div className="mt-6 mb-6 flex justify-between">
                <p className="text-lg font-semibold">Total:</p>
                {orderData.Order_data.length > 0 ? (
                  <p className="text-lg">{formatCurrency(totalPrice)}</p>
                ) : (
                  <p className="text-lg">N/A</p>
                )}
              </div>
              <div className="flex justify-center space-x-4">

                {buktiPembayaran ? (
                  <button onClick={() => handleLihatConfirmation()} className="py-3 px-6 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">
                    Lihat Bukti Pembayaran
                  </button>
                ) : (
                  <Link to={`/payment-confirmation/${id}`}>
                    <button className="py-3 px-6 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">
                      Konfirmasi Pembayaran
                    </button>
                  </Link>
                )}

              </div>
              <div className="mt-6 text-gray-700 mb-2">Thank you for your business!</div>
            </div>
          )}
        </div>
      </div>

      {showAlert && (
        <div className="bg-red-500 text-white py-3 px-4 rounded-md absolute top-3 right-3">
          {alertMessage}
        </div>
      )}

      {showModal ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="text-3xl font-semibold">
                    Bukti Pembayaran
                  </h3>
                </div>
                <div className="relative p-6 flex-auto">
                  <div className="mb-4">
                    {/* Render payment confirmation details here */}
                    <p>
                      Created At: {formatDate(buktiPembayaran.CreatedAt)}
                    </p>
                    <p>
                      Evidence: {buktiPembayaran.Evidence}
                    </p>
                    <p>
                      Bank: {buktiPembayaran.Name_bank}
                    </p>
                    <p>
                      No Rekening: {buktiPembayaran.No_rek}
                    </p>
                    {/* Add other payment confirmation details as needed */}
                  </div>
                  {buktiPembayaran.Evidence && (
                    <div>
                      {buktiPembayaran.Evidence.endsWith('.pdf') ? (
                        // Render PDF viewer or link
                        <embed
                          src={`${apiUrl}/files/${buktiPembayaran.Evidence}`}  // Assuming Evidence is a file path or URL
                          type="application/pdf"
                          width="100%"
                          height="600px"
                        />
                      ) : (
                        // Render image
                        <img
                          src={`${apiUrl}/files/${buktiPembayaran.Evidence}`}  // Assuming Evidence is a file path or URL
                          alt="Bukti Pembayaran"
                          className="max-w-full h-auto rounded-lg"
                        />
                      )}
                    </div>
                  )}
                </div>
                <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                  <button className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button" onClick={() => setShowModal(false)}>
                    Close
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
      <Footer />
    </>
  );
}

export default Invoice
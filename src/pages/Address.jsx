import React, { useEffect, useState } from 'react'
import Header from '../components/Header';
import Footer from '../components/Footer';
import avatar from './../../public/assets/images/avatar.png'
import product5 from './../../public/assets/images/products/product5.jpg'
import product6 from './../../public/assets/images/products/product6.jpg'
import product10 from './../../public/assets/images/products/product10.jpg'
import { geAddressByUser, changeDefaultAddress } from '../services/address';
import Sidebar from '../components/Sidebar';
import { Link, useNavigate } from "react-router-dom";
import jwt_decode from "jwt-decode";

const Address = () => {
    const navigate = useNavigate();
    const [address, setAddress] = useState('');
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const token = localStorage.getItem('token');
    useEffect(() => {
        if (!token) {
            navigate('/login');
        }
        viewAddress();
    }, [currentPage, token ]);

    const viewAddress = async () => {
      
        try {  
          if (token) {
            const decode = jwt_decode(token);
            let data = {
              id_user       : decode.id
            }
            const response = await geAddressByUser(token, data, currentPage);
            setAddress(response.data.address)
            setCurrentPage(response.data.currentPage);
            setTotalPages(response.data.totalPages);
            console.log(response);
          }     
  
        } catch (error) {
          console.error('Error fetching provinsi data:', error);
        }
    };
    const handlePagination = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    const handleDefaultAddress = async (id) => {
      
        try {  
          if (token) {
            const decode = jwt_decode(token);
            let data = {
              id_user       : decode.id
            }
            const response = await changeDefaultAddress(token, id, data);
            console.log(response);
            viewAddress();
          }     
  
        } catch (error) {
          console.error('Error fetching provinsi data:', error);
        }
    };

    return (
        <>
        <Header />
            <div className="container py-4 flex items-center gap-3">
                <a href="../index.html" className="text-primary text-base">
                    <i className="fa-solid fa-house"></i>
                </a>
                <span className="text-sm text-gray-400">
                    <i className="fa-solid fa-chevron-right"></i>
                </span>
                <p className="text-gray-600 font-medium">Address</p>
            </div>
            <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">

                <Sidebar />

                <div className="col-span-9 space-y-4">
                    <Link to="/create-address" className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Tambah Alamat</Link>

                    {address.length === 0 ? (
                        <div className="flex items-center justify-center h-screen">
                            <div className="text-center">
                                <h2 className="text-3xl font-semibold mb-4">Your Address is Empty</h2>
                                <p className="text-gray-500 mb-8">Looks like you haven't added any items to your address yet.</p>
                                <Link to="/create-address">
                                    <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Tambahkan Now</button>
                                </Link>                            
                            </div>
                        </div>
                    ) : (
                        <>
                            {address.map(address => (                                                         
                                <div key={address.ID} className="flex items-center justify-between border gap-6 p-4 border-gray-200 rounded">
                                    <div className="w-1/3">
                                        <h2 className="text-gray-800 text-xl font-medium uppercase">{address.Prov.ProvName}, {address.City.CityName}, {address.Dis.DisName}, {address.Subdis.SubdisName}, {address.Postal_code}</h2>
                                        <p className="text-gray-500 text-sm">Patokan : <span className="text-green-600">{address.Address}</span></p>
                                        <p className="text-gray-500 text-sm">Telp : <span className="text-green-600">{address.No_telp}</span></p>
                                    </div>
                                    {address.Is_active == 1 ? (
                                        <>
                                            <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Default</button>
                                        </>
                                    ) : (
                                        <>                                        
                                            <button onClick={() => handleDefaultAddress(address.ID)} className="px-6 py-2 text-center text-sm text-black bg-default border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Jadikan Default</button> 
                                        </>   
                                    )}


                                    <div className="text-gray-600 cursor-pointer hover:text-primary">
                                        <i className="fa-solid fa-trash"></i>
                                    </div>
                                </div>
                            ))}
                        </>          
                    )}

                    
                    {/* Pagination */}
                    {totalPages > 1 && (
                        <div className="flex justify-center mt-4">
                        <ul className="flex space-x-2">
                            {[...Array(totalPages).keys()].map((pageNumber) => (
                            <li key={pageNumber}>
                                <button
                                className={`px-3 py-1 rounded-md ${
                                    pageNumber + 1 === currentPage ? 'bg-gray-600 text-white' : 'bg-gray-200'
                                }`}
                                onClick={() => handlePagination(pageNumber + 1)}
                                >   
                                {pageNumber + 1}
                                </button>
                            </li>
                            ))}
                        </ul>
                        </div>
                    )}
                </div>

            </div>
        <Footer />
        </>
    );
}

export default Address
import React, { useEffect, useState } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Sidebar from '../components/Sidebar';
import { useNavigate, useParams } from 'react-router-dom';
import { createPaymentConfirmation } from '../services/payment_confirmations';
import jwt_decode from "jwt-decode";

const PaymentConfirmation = () => {
    const navigate = useNavigate();
    const { id } = useParams();
    const [selectedFile, setSelectedFile] = useState(null);
    const [formData, setFormData] = useState({
        Name_bank: '',
        No_rek: '',
        Evidence: null
    });
    const [token, setToken] = useState('');
    const [showAlert, setShowAlert] = useState(false);
    const [alertMessage, setAlertMessage] = useState('');

    useEffect(() => {
        const userToken = localStorage.getItem('token');
        if (!userToken) {
            navigate('/login');
        } else {
            setToken(userToken);
        }
    }, [navigate]);

    const handleFileChange = (e) => {
        const file = e.target.files[0];
        setSelectedFile(file);

        setFormData((prevFormData) => ({
            ...prevFormData,
            Evidence: file
        }));
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;

        setFormData((prevFormData) => ({
            ...prevFormData,
            [name]: value
        }));
    };

    const fetchConfirmation = async (decode, id, token, formData) => {
        try {
            await createPaymentConfirmation(decode, id, token, formData);
            setAlertMessage('Payment confirmation successful');
            setShowAlert(true);
            setTimeout(() => {
                setShowAlert(false);
                setAlertMessage('');
                navigate(`/invoice/${id}`);
            }, 500);
            console.log('Payment confirmation successful');
        } catch (error) {
            console.error('Error creating payment confirmation:', error);
        }
    };

    const handleConfirmation = () => {
        try {
            if (token) {
                const decode = jwt_decode(token);
                fetchConfirmation(decode, id, token, formData);
            } else {
                setAlertMessage('Silahkan Login Terlebih Dahulu');
                setShowAlert(true);
                setTimeout(() => {
                    setShowAlert(false);
                    setAlertMessage('');
                }, 2000);
            }
        } catch (error) {
            console.error('Error placing order:', error);
        }
    };

    return (
        <>
            <Header />
            <div className="container py-4 flex items-center gap-3">
                <a href="#" className="text-primary text-base">
                    <i className="fa-solid fa-house"></i>
                </a>
                <span className="text-sm text-gray-400">
                    <i className="fa-solid fa-chevron-right"></i>
                </span>
                <p className="text-gray-600 font-medium">Payment Confirmation</p>
            </div>

            <div className="container grid grid-cols-12 items-start gap-6 pt-4 pb-16">
                <Sidebar />

                <div className="col-span-9 shadow rounded px-6 pt-5 pb-7">
                    <h4 className="text-lg font-medium capitalize mb-4">Payment Confirmation</h4>
                    <div className="space-y-4">
                        <div className="grid grid-cols-2 gap-4">
                            <div>
                                <label htmlFor="Name_bank">Nama Bank</label>
                                <input
                                    type="text"
                                    name="Name_bank"
                                    id="Name_bank"
                                    className="input-box"
                                    value={formData.Name_bank}
                                    onChange={handleInputChange}
                                />
                            </div>
                            <div>
                                <label htmlFor="No_rek">Nomor Rekening</label>
                                <input
                                    type="text"
                                    name="No_rek"
                                    id="No_rek"
                                    className="input-box"
                                    value={formData.No_rek}
                                    onChange={handleInputChange}
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-2 gap-4">
                            <div>
                                <label htmlFor="Evidence">Bukti Transfer</label>
                                <div className="flex items-center gap-2">
                                    <input
                                        type="file"
                                        name="Evidence"
                                        id="Evidence"
                                        className="hidden"
                                        onChange={handleFileChange}
                                    />
                                    <label htmlFor="Evidence" className="py-1 px-5 text-sm font-medium text-center rounded-lg bg-primary-700 border border-gray-300 sm:w-fit hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-primary-500 dark:focus:border-primary-500">
                                        Browse
                                    </label>
                                    {selectedFile && (
                                        <span className="text-gray-500">{selectedFile.name}</span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="mt-5">
                        <button
                            type="button"
                            onClick={handleConfirmation}
                            className="py-3 px-4 text-center text-white bg-primary border border-primary rounded-md hover:bg-transparent hover:text-primary transition font-medium">
                            Confirmation
                        </button>
                    </div>
                </div>
            </div>

            {showAlert && (
                <div className="bg-red-500 text-white py-3 px-4 rounded-md absolute top-3 right-3">
                    {alertMessage}
                </div>
            )}
            <Footer />
        </>
    );
};

export default PaymentConfirmation;

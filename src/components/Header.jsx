import React, { useState, useEffect } from 'react'

import logo from './../../public/assets/images/logo-daridanke.png'
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from 'react-redux';
import jwt_decode from "jwt-decode";
import { formatCurrency } from '../helpers/helpers'; 


const Header = ({ onSearch }) => {
    const [searchTerm, setSearchTerm] = useState('');
    const [cartItemsCount, setCartItemsCount] = useState(0);
    const storedCartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    const cartItemsCountsRedux = useSelector(state => state.cart.cartItems.length);
    const [isCartOpen, setIsCartOpen] = useState(false);
    const navigate = useNavigate();
    const token = localStorage.getItem('token');
    const apiUrl = import.meta.env.VITE_REACT_APP_API_URL_DASH;
    const [is_Sticky, setIsSticky] = useState(false);

    const handleSearch = () => {
        onSearch(searchTerm);
    };

    const handleOnChange = (e) => {
        setSearchTerm(e.target.value);
        if (window.location.pathname !== '/shop') {
            navigate('/shop');
        }
    }
    

    const handleKeyPress = (e) => {
        // Memeriksa jika tombol yang ditekan adalah tombol "Enter"
        if (e.key === 'Enter') {
            handleSearch(searchTerm); // Menjalankan pencarian saat tombol "Enter" ditekan
        }
    };

    const handleMouseEnter = () => {
        // Mengarahkan pengguna ke halaman toko saat mouse masuk ke input
        navigate('/shop');
    };

    const isSticky = (e) => {
        const header__center = document.querySelector(".header__center");
        const scrollTop = window.scrollY;
        if (scrollTop >= 100) {
            header__center.classList.add("is-sticky");
            setIsSticky(true);
        } else {
            header__center.classList.remove("is-sticky");
            setIsSticky(false);
        }
    };

    const handleCartClick = (e) => {
        e.preventDefault();
        toggleCart();
    };

    const toggleCart = () => {
        setIsCartOpen(!isCartOpen);
    };
    
    const checkTokenExpiration = () => {
        if (token) {
            const currentTime = Date.now() / 1000; // Waktu saat ini dalam detik
            const decoded = jwt_decode(token)
            if (decoded.exp < currentTime) {
                console.log('Token expired');
                localStorage.removeItem('token');
            } else {
                console.log('Token is valid');
            }
        } else {
            console.log('No token found');
            // Token tidak ditemukan di localStorage
        }
    };
    
    useEffect(() => {
        checkTokenExpiration();

        if (cartItemsCountsRedux > 0) {
            setCartItemsCount(cartItemsCountsRedux);
        } else {
            // Gunakan jumlah item dari local storage jika tidak ada dalam Redux
            setCartItemsCount(storedCartItems.length);
        }

        window.addEventListener("scroll", isSticky);
        return () => {
            window.removeEventListener("scroll", isSticky);
        };
    }, [cartItemsCountsRedux, storedCartItems]);

    const totalCartPrice = storedCartItems.reduce((total, item) => total + item.totalPrice, 0);

    return (
        <>      
            <header className="py-4 shadow-sm bg-white">
                <div className="container flex items-center justify-between">
                    <Link to="/">
                        <img src={logo} alt="Logo" className="w-32" />
                    </Link>

                    <div className="w-full max-w-xl relative flex transition duration-300 ease-in-out hover:scale-105">
                        <span className="absolute left-4 top-3 text-lg text-gray-400">
                            <i className="fa-solid fa-magnifying-glass"></i>
                        </span>
                        <input type="text" name="search" id="search" value={searchTerm} onChange={handleOnChange} className="w-full border border-primary border-r-0 pl-12 py-3 pr-3 rounded-l-md focus:outline-none" placeholder="search" />
                        {/* <input type="text" name="search" id="search" value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} onMouseEnter={handleMouseEnter} onKeyDown={handleKeyPress} className="w-full border border-primary border-r-0 pl-12 py-3 pr-3 rounded-l-md focus:outline-none" placeholder="search" /> */}
                        <button className="bg-primary border border-primary text-white px-8 rounded-r-md hover:bg-transparent hover:text-primary transition" onClick={handleSearch}>Search</button>
                    </div>

                    <div className="flex items-center space-x-4">
                    
                    <Link to="/wishlist">
                        <div className="text-center text-gray-700 hover:text-primary transition relative">
                            <div className="text-2xl">
                                <i className="fa-regular fa-heart"></i>
                            </div>
                            <div className="text-xs leading-3">Wishlist</div>
                            {/* <div className="absolute right-0 -top-1 w-5 h-5 rounded-full flex items-center justify-center bg-primary text-white text-xs">8</div> */}
                        </div>
                    </Link>
                    <div className="text-center text-gray-700 hover:text-primary transition relative cursor-pointer" onClick={handleCartClick}>
                        <div className="text-2xl">
                            <i className="fa-solid fa-bag-shopping"></i>
                        </div>
                        <div className="text-xs leading-3">Cart</div>
                        <div className="absolute -right-3 -top-1 w-5 h-5 rounded-full flex items-center justify-center bg-primary text-white text-xs">{cartItemsCount}</div>
                    </div>
                    <Link to="/account">
                        <div className="text-center text-gray-700 hover:text-primary transition relative">
                            <div className="text-2xl">
                                <i className="fa-regular fa-user"></i>
                            </div>
                            {token ? (
                                <div className="text-xs leading-3">{jwt_decode(token).username}</div>
                            ) : (
                                <div className="text-xs leading-3">Account</div>
                            )}
                        </div>
                    </Link>
                    </div>
                </div>
            </header>
            
            {/* <nav className="bg-gray-800 header__center">
                <div className="container flex">
                    <div className="px-8 py-4 bg-primary md:flex items-center cursor-pointer relative group">
                        <span className="text-white">
                            <i className="fa-solid fa-bars"></i>
                        </span> */}
                        {/* <span className="capitalize ml-2 text-white hidden">All Categories</span> */}

                        {/* <div className="absolute w-full left-0 top-full bg-white shadow-md py-3 divide-y divide-gray-300 divide-dashed opacity-0 group-hover:opacity-100 transition duration-300 invisible group-hover:visible">
                            <a href="#" className="flex items-center px-6 py-3 hover:bg-gray-100 transition">
                                <img src={sofa} alt="sofa" className="w-5 h-5 object-contain" />
                                <span className="ml-6 text-gray-600 text-sm">Sofa</span>
                            </a>
                            <a href="#" className="flex items-center px-6 py-3 hover:bg-gray-100 transition">
                                <img src={terrace} alt="terrace" className="w-5 h-5 object-contain" />
                                <span className="ml-6 text-gray-600 text-sm">Terarce</span>
                            </a>
                            <a href="#" className="flex items-center px-6 py-3 hover:bg-gray-100 transition">
                                <img src={bed} alt="bed" className="w-5 h-5 object-contain" />
                                <span className="ml-6 text-gray-600 text-sm">Bed</span>
                            </a>
                            <a href="#" className="flex items-center px-6 py-3 hover:bg-gray-100 transition">
                                <img src={office} alt="office" className="w-5 h-5 object-contain" />
                                <span className="ml-6 text-gray-600 text-sm">office</span>
                            </a>
                            <a href="#" className="flex items-center px-6 py-3 hover:bg-gray-100 transition">
                                <img src={outdoorcafe} alt="outdoor" className="w-5 h-5 object-contain" />
                                <span className="ml-6 text-gray-600 text-sm">Outdoor</span>
                            </a>
                            <a href="#" className="flex items-center px-6 py-3 hover:bg-gray-100 transition">
                                <img src={bed2} alt="Mattress" className="w-5 h-5 object-contain" />
                                <span className="ml-6 text-gray-600 text-sm">Mattress</span>
                            </a>
                        </div> */}
                    {/* </div>

                    <div className="items-center justify-between flex-grow md:pl-12 py-5 md:flex hidden">
                        <div className="flex items-center space-x-6 capitalize">
                        <Link to="/">
                            <div className="text-gray-200 hover:text-white transition">Home</div>
                        </Link>
                            <Link to="/shop">
                            <div className="text-gray-200 hover:text-white transition">Shop</div>
                            </Link>
                            <Link to="/about-as" className="text-gray-200 hover:text-white transition">Tentang Kami</Link>
                            <Link to="/contact" className="text-gray-200 hover:text-white transition">Contact</Link>
                        </div>
                        {token ? (
                            <Link to="/account">
                                <div className="text-gray-200 hover:text-white transition">Dashboard</div>
                            </Link>
                        ) : (
                            <Link to="/login">
                                <div className="text-gray-200 hover:text-white transition">Login</div>
                            </Link>
                        )}
                    </div>
                </div>
            </nav> */}

            <nav className="bg-gray-800 header__center fixed left-0 w-full z-50 shadow-md transition-all">
                <div className="container flex justify-between items-center py-3">
                    
                    <div className="flex items-center space-x-6 capitalize ml-10">
                        <Link to="/" className="text-gray-200 hover:text-white transition">Home</Link>
                        <Link to="/shop" className="text-gray-200 hover:text-white transition">Shop</Link>
                        <Link to="/about-as" className="text-gray-200 hover:text-white transition">Tentang Kami</Link>
                        <Link to="/contact" className="text-gray-200 hover:text-white transition">Contact</Link>
                    </div>
                    {is_Sticky && (
                    <div className="flex items-center space-x-4">
                        <Link to="/wishlist">
                            <div className="text-center text-gray-200 hover:text-white transition relative">
                            <div className="text-1xl">
                                <i className="fa-regular fa-heart"></i>
                            </div>
                            <div className="text-xs leading-3">Wishlist</div>
                            </div>
                        </Link>
                        <div className="text-center text-gray-200 hover:text-white transition relative cursor-pointer" onClick={handleCartClick}>
                            <div className="text-1xl">
                            <i className="fa-solid fa-bag-shopping"></i>
                            </div>
                            <div className="text-xs leading-3">Cart</div>
                            <div className="absolute -right-3 -top-1 w-5 h-5 rounded-full flex items-center justify-center bg-primary text-white text-xs">{cartItemsCount}</div>
                        </div>
                    </div>
                    )}
                </div>
            </nav>
           
            {/* {isCartOpen && (
                <div className="fixed right-0 top-0 bottom-0 w-80 bg-white border border-gray-200 p-4 rounded shadow-md overflow-y-auto z-50">
                    <div className="flex justify-between items-center mb-4">
                        <h2 className="text-xl font-semibold mb-2">Shopping Cart</h2>
                        <button className="text-gray-500 hover:text-gray-600 focus:outline-none" onClick={toggleCart}>
                            <i className="fas fa-times"></i>
                        </button>
                    </div>

                    {storedCartItems.length === 0 ? ( // Periksa apakah keranjang belanja kosong
                    <div className="flex items-center justify-center h-screen">
                        <div className="text-center">
                            <h2 className="text-3xl font-semibold mb-4">Your Cart is Empty</h2>
                            <p className="text-gray-500 mb-8">Looks like you haven't added any items to your cart yet.</p>
                            <Link to="/shop">
                                <button className="px-6 py-2 text-center text-sm text-white bg-primary border border-primary rounded hover:bg-transparent hover:text-primary transition uppercase font-roboto font-medium">Shop Now</button>
                            </Link>                            
                        </div>
                    </div>
                    ) : (
                    <>
                        {storedCartItems.map((item, index) => (
                        <div className="flex justify-between items-center mb-4" key={index}>
                            <img src={`${apiUrl}/files/${item.Image}`} alt="Product" className="w-16 h-16" />
                            <div className="">
                                <p className="font-semibold">{item.Name}</p>
                                <p className="text-sm text-gray-500">Price: {formatCurrency(item.totalPrice)}</p>
                                <p className="text-sm text-gray-500">Quantity: {item.quantity}</p>
                            </div>
                            <button className="text-red-500 hover:text-red-600 focus:outline-none">
                                <i className="fa-solid fa-trash"></i>
                            </button>
                        </div>
                        ))}
                        <div className="flex justify-between items-center mb-10">
                            <p className="text-xl font-semibold">Total:</p>
                            <p className="text-xl font-semibold">{formatCurrency(totalCartPrice)}</p>
                        </div>
                        <Link to="/checkout">
                            <button type="submit" className="py-3 px-5 text-sm font-medium text-center rounded-lg bg-primary-700 border border-gray-300 sm:w-fit hover:bg-primary-800 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:ring-primary-500 dark:focus:border-primary-500">Checkout</button>
                        </Link>
                    </>            
                )}
                </div>                
            )} */}
            {isCartOpen && (
                <div className="fixed right-0 top-0 bottom-0 w-96 bg-white shadow-lg border-l border-gray-300 p-6 rounded-l-2xl overflow-y-auto z-50 flex flex-col">
                    <div className="flex justify-between items-center mb-6 border-b pb-4">
                        <h2 className="text-2xl font-bold text-gray-800">Shopping Cart</h2>
                        <button className="text-gray-400 hover:text-gray-600 focus:outline-none" onClick={toggleCart}>
                            <i className="fas fa-times text-xl"></i>
                        </button>
                    </div>

                    {storedCartItems.length === 0 ? (
                        <div className="flex flex-col items-center justify-center flex-grow">
                            <h2 className="text-2xl font-semibold text-gray-700">Your Cart is Empty</h2>
                            <p className="text-gray-500 mb-6 text-center">Looks like you haven't added anything yet.</p>
                            <Link to="/shop">
                                <button className="px-6 py-3 bg-blue-600 text-white font-medium rounded-lg hover:bg-blue-700 transition-all">Shop Now</button>
                            </Link>
                        </div>
                    ) : (
                        <>
                            <div className="flex-grow space-y-6 overflow-y-auto">
                                {storedCartItems.map((item, index) => (
                                    <div className="flex items-center justify-between bg-gray-100 p-4 rounded-lg" key={index}>
                                        <img src={`${apiUrl}/files/${item.Image}`} alt="Product" className="w-16 h-16 rounded-md object-cover" />
                                        <div className="flex-1 ml-4">
                                            <p className="font-medium text-gray-800">{item.Name}</p>
                                            <p className="text-sm text-gray-500">{formatCurrency(item.totalPrice)} | Qty: {item.quantity}</p>
                                        </div>
                                        <button className="text-red-500 hover:text-red-700 focus:outline-none">
                                            <i className="fa-solid fa-trash text-lg"></i>
                                        </button>
                                    </div>
                                ))}
                            </div>

                            <div className="mt-6 border-t pt-4">
                                <div className="flex justify-between items-center text-lg font-semibold text-gray-800">
                                    <span>Total:</span>
                                    <span>{formatCurrency(totalCartPrice)}</span>
                                </div>
                                <Link to="/checkout" className="block mt-6">
                                    <button className="w-full py-3 bg-gray-900 text-white font-semibold rounded-lg hover:bg-gray-700 transition-all">Checkout</button>
                                </Link>
                            </div>
                        </>
                    )}
                </div>
            )}
        </>
    );
};

export default Header;
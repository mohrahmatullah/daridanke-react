import React from 'react';
import logo_footer from './../../public/assets/images/logo-daridanke.png';
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <>
      <footer className="bg-gray-900 text-white pt-16 pb-10">
        <div className="container mx-auto grid grid-cols-1 md:grid-cols-4 gap-8 px-6">
          {/* Logo & Deskripsi */}
          <div>
            <img src={logo_footer} alt="logo" className="w-[160px] mb-4" />
            <p className="text-gray-400 text-sm">
              Platform solusi bisnis profesional dengan legalitas yang terpercaya.
            </p>
          </div>

          {/* Informasi */}
          <div>
            <h3 className="text-lg font-semibold text-gray-200">Informasi</h3>
            <ul className="mt-4 space-y-3">
              <li><Link to="/about-as" className="hover:text-gray-300 transition">Tentang Kami</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Syarat & Ketentuan</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Bantuan</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Kebijakan Privasi</Link></li>
            </ul>
          </div>

          {/* Produk */}
          <div>
            <h3 className="text-lg font-semibold text-gray-200">Produk</h3>
            <ul className="mt-4 space-y-3">
              <li><Link to="/" className="hover:text-gray-300 transition">Prosedur</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Formulir</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Legalitas</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Proposal & Kontrak</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Presentasi PowerPoint</Link></li>
            </ul>
          </div>

          {/* Social Media */}
          <div>
            <h3 className="text-lg font-semibold text-gray-200">Hubungi Kami</h3>
            <ul className="mt-4 space-y-3">
              <li><Link to="/" className="hover:text-gray-300 transition">Karir</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Blog</Link></li>
              <li><Link to="/" className="hover:text-gray-300 transition">Kritik & Saran</Link></li>
            </ul>

            {/* Ikon Social Media */}
            <div className="flex space-x-4 mt-6">
              <a href="#" className="text-gray-400 hover:text-gray-300 text-2xl transition">
                <i className="fa-brands fa-facebook"></i>
              </a>
              <a href="#" className="text-gray-400 hover:text-gray-300 text-2xl transition">
                <i className="fa-brands fa-instagram"></i>
              </a>
              <a href="#" className="text-gray-400 hover:text-gray-300 text-2xl transition">
                <i className="fa-brands fa-twitter"></i>
              </a>
              <a href="#" className="text-gray-400 hover:text-gray-300 text-2xl transition">
                <i className="fa-brands fa-github"></i>
              </a>
            </div>
          </div>
        </div>
      </footer>

      {/* Footer Copyright */}
      <div className="bg-gray-800 py-4 border-t border-gray-700">
        <div className="container mx-auto text-center text-gray-400 text-sm">
          &copy; 2025 Daridanke - All Rights Reserved
        </div>
      </div>
    </>
  );
};

export default Footer;

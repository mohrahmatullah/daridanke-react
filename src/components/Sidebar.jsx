import React from 'react'
import { Link, useNavigate } from "react-router-dom";
import jwt_decode from "jwt-decode";

const Sidebar = () => {
    const navigate = useNavigate();
    const token = localStorage.getItem('token');
    const logout = () => {
        localStorage.removeItem('token');
        navigate('/login');
    };

    return (
        <>
            <div className="col-span-3">
                <div className="px-4 py-3 shadow flex items-center gap-4">
                    <div className="flex-shrink-0">
                        <img src="../assets/images/avatar.png" alt="profile" className="rounded-full w-14 h-14 border border-gray-200 p-1 object-cover" />
                    </div>
                    <div className="flex-grow">
                        <p className="text-gray-600">Hello,</p>
                        {token ? (
                            <h4 className="text-gray-800 font-medium">{jwt_decode(token).username}</h4>
                        ) : (
                            <h4 className="text-gray-800 font-medium">Default</h4>
                        )}
                    </div>
                </div>

                <div className="mt-6 bg-white shadow rounded p-4 divide-y divide-gray-200 space-y-4 text-gray-600">
                    <div className="space-y-1 pl-8">                        
                        <Link to="/account" className="relative text-primary block font-medium capitalize transition">
                            <span className="absolute -left-8 top-0 text-base">
                                <i className="fa-regular fa-address-card"></i>
                            </span>
                            Profile information
                        </Link>
                        <Link to="/profile" className="relative hover:text-primary block capitalize transition">
                            Manage account
                        </Link>
                        <Link to="/address" className="relative hover:text-primary block capitalize transition">
                            Manage addresses
                        </Link>
                        <a href="#" className="relative hover:text-primary block capitalize transition">
                            Change password
                        </a>
                    </div>

                    <div className="space-y-1 pl-8 pt-4">
                        <Link to="/order/all" className="relative hover:text-primary block font-medium capitalize transition">
                            <span className="absolute -left-8 top-0 text-base">
                                <i className="fa-solid fa-box-archive"></i>
                            </span>
                            My order history
                        </Link>
                        <Link to="/order/pending" className="relative hover:text-primary block capitalize transition">
                            My returns
                        </Link>
                        <Link to="/order/cancel" className="relative hover:text-primary block capitalize transition">
                            My Cancellations
                        </Link>
                        <Link to="/order/reviews" className="relative hover:text-primary block capitalize transition">
                            My reviews
                        </Link>
                    </div>

                    <div className="space-y-1 pl-8 pt-4">
                        {/* <a href="#" className="relative hover:text-primary block font-medium capitalize transition">
                            <span className="absolute -left-8 top-0 text-base">
                                <i className="fa-regular fa-credit-card"></i>
                            </span>
                            Payment methods
                        </a> */}
                        <Link to="/voucher" className="relative hover:text-primary block capitalize transition">                            
                            <span className="absolute -left-8 top-0 text-base">
                                <i className="fa-regular fa-credit-card"></i>
                            </span>
                            Voucher
                        </Link>
                    </div>

                    <div className="space-y-1 pl-8 pt-4">
                        <Link to="/wishlist" className="relative hover:text-primary block font-medium capitalize transition">
                            <span className="absolute -left-8 top-0 text-base">
                                <i className="fa-regular fa-heart"></i>
                            </span>
                            My wishlist
                        </Link>
                    </div>

                    <div className="space-y-1 pl-8 pt-4">
                        <a href="#" onClick={logout} className="relative hover:text-primary block font-medium capitalize transition">
                            <span className="absolute -left-8 top-0 text-base">
                                <i className="fas fa-power-off"></i>
                            </span>
                            Logout
                        </a>
                    </div>

                </div>
            </div>
        </>
    );
}

export default Sidebar
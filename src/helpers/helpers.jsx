// Function to format date as d-m-Y H:i:s in Jakarta time zone
export function formatDate(dateString) {
  const date = new Date(dateString);
  const options = {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      second: '2-digit',
      timeZone: 'Asia/Jakarta', // Waktu Indonesia Barat (WIB)
  };
  return new Intl.DateTimeFormat('en-US', options).format(date);
}

// Function to format number to Indonesian Rupiah (IDR)
export function formatCurrency(amount) {
  const formatter = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 0,
  });
  return `${formatter.format(amount)}`;
}
